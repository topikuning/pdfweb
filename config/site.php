<?php 

return [
    'site_url' => 'http://www.nama.situs',
    'node_server_url' => 'http://localhost:4000',
    'keywords_per_scrap' => '500',
    'engine_scrap' => 'pdfgoogle', //pdfbing, wordbing, yandex
    'run_scrap_at' => '23:00',
    'min_file_size' => 100000 //minimum file size
];
