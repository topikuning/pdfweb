<?php
	$sitemapurl = 'http://'.$_SERVER['SERVER_NAME'].'/sitemap.xml';
	$services = array('google','bing','yandex','baidu');
	foreach ($services as $toping) {
		if ($toping == 'google') {
			$pingurl = 'http://www.google.com/webmasters/sitemaps/ping?sitemap='.$sitemapurl;
			$curl_handle=curl_init();
			curl_setopt($curl_handle,CURLOPT_URL,$pingurl);
			curl_setopt($curl_handle, CURLOPT_PROXY, "127.0.0.1:9050");
			curl_setopt($curl_handle, CURLOPT_PROXYTYPE, 7);
			curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
			curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
			$buffer = curl_exec($curl_handle);
			curl_close($curl_handle);
		}
		elseif($toping == 'bing') {
			$pingurl = 'http://www.bing.com/webmaster/ping.aspx?siteMap='.$sitemapurl;
			$curl_handle=curl_init();
			curl_setopt($curl_handle,CURLOPT_URL,$pingurl);
			curl_setopt($curl_handle, CURLOPT_PROXY, "127.0.0.1:9050");
			curl_setopt($curl_handle, CURLOPT_PROXYTYPE, 7);
			curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
			curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
			$buffer = curl_exec($curl_handle);
			curl_close($curl_handle);
		}
		elseif($toping == 'yandex') {
			$pingurl = 'http://blogs.yandex.ru/pings/?status=success&url='.$sitemapurl;
			$curl_handle=curl_init();
			curl_setopt($curl_handle,CURLOPT_URL,$pingurl);
			curl_setopt($curl_handle, CURLOPT_PROXY, "127.0.0.1:9050");
			curl_setopt($curl_handle, CURLOPT_PROXYTYPE, 7);
			curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
			curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
			$buffer = curl_exec($curl_handle);
			curl_close($curl_handle);
		}
		elseif($toping == 'baidu') {
			$request = xmlrpc_encode_request("weblogUpdates.ping", array("PDF Search Engine", $sitemapurl) );
			$header[] = "Host: ping.baidu.com";
			$header[] = "Content-type: text/xml";
			$header[] = "Content-length: ".strlen($request) . "\r\n";
			$header[] = $request;
			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, "http://ping.baidu.com/ping/RPC2");
			curl_setopt( $ch, CURLOPT_PROXY, "127.0.0.1:9050");
			curl_setopt( $ch, CURLOPT_PROXYTYPE, 7);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
			curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
			$result = curl_exec( $ch );
			curl_close($ch);
		}
	}
?>