<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->engine = 'MyISAM'; // means you can't use foreign key constraints
            $table->increments('id');
			$table->string('title'); 
			$table->string('keyword');
			$table->string('abstract'); 
			$table->string('author_name'); 
			$table->string('author_avatar');
			$table->string('slug')->unique(); 
			$table->boolean('published')->index()->default('1');
			$table->text('body'); 
			$table->integer('viewed')->unsigned(); 
			$table->integer('download')->unsigned()->index();
			$table->timestamps();
		});

        DB::statement('ALTER TABLE posts ADD FULLTEXT search(title, keyword, abstract)');

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('posts', function($table) {
            $table->dropIndex('search');
        });

    	Schema::drop('posts');
	}

}
