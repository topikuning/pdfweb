<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToCreatedAtRecentQuerys extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recent_querys', function(Blueprint $table)
		{
			$table->index('created_at');
		});
		Schema::table('recent_viewed', function(Blueprint $table)
		{
			$table->index('created_at');
		});
		Schema::table('recent_download', function(Blueprint $table)
		{
			$table->index('created_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recent_querys', function(Blueprint $table)
		{
			//
		});
	}

}
