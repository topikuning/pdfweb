<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('PostSeed');
	}

}


class PostSeed extends Seeder {

    public function run()
    {
        $data = Db::table('posts')->get();
        foreach($data as $row){
              $update = [
                'slug_keyword' => str_slug($row->keyword)
              ];
              DB::table('posts')->where('id', $row->id)->update($update);
        }
    }

}


class RecentSeeder extends Seeder {

    public function run()
    {
        DB::table('recent_querys')->truncate();
        $data = Db::table('posts')->groupBy('keyword')->get();
        foreach($data as $row){
		      $recent = [
		        'title' => $row->keyword,
		        'slug' => str_slug($row->keyword),
		        'created_at' => DB::raw('now()')
		      ];
		      DB::table('recent_querys')->insert($recent);
        }
    }

}
