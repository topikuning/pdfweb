<!doctype html>
<html class="no-js" lang="en-US" itemscope itemtype="http://schema.org/WebPage">
  @include('partials._header')
  <body>
    @include('partials._frontnav')
    @yield('content')
    @include('partials._footer')
  </body>
</html>
