@extends('admin')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pages
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li class="active">Pages</li>
  </ol>
</section>

<section class="content">
     @include('partials._message')
     <div class="row">
         <div class="col-md-12">
             <div class="box box-info" id="box_page">
                 <div class="box-header with-header">
                    <a href="{{ url('badmin/pages/create') }}" class="btn btn-sm btn-info" id="add_new"><i class="fa fa-plus"></i> Add New</a>
                 </div>
                 <div class="box-body">
                    <table class="table table-hover" id="page_table">
                        <thead>
                            <tr>
                                <th style="width:30px;">ID</th>
                                <th style="width:75px;">Published</th>
                                <th>Title</th>
                                <th>Slug</th>                                
                                <th style="width:75px;">Action</th>
                            </tr>
                        </thead>
                    </table>
                 </div>
             </div>
         </div>
     </div>
</section>

<script type="text/javascript">
    $(document).ready(function(){
       function editAttribute(id,field,value){
            $('#box_page').append(_loading);
            var url = base_url + '/badmin/pages/attribute'; 
            var params = {
                id:id,
                field:field,
                value:value
            };
            $.post(url,params, function(res){
                $('#ajax-loading').remove();
            });
        }

        $('#page_table').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax":  base_url + "/badmin/pages/read",
            "columnDefs":[
                { 'bSortable': false, 'aTargets': [4] },
                {
                     render: function ( data, type, row ) {

                        if(data =="1")
                        return '<input data-id="'+row[0]+'" type="checkbox" name="published" checked>';
                        return '<input data-id="'+row[0]+'" type="checkbox" name="published">';
                     },

                     "targets": 1
                }
            ],
            "fnDrawCallback":function(){
               $('input[name="published"]').bootstrapSwitch({size:'mini'});
                $('input[name="published"]').on('switchChange.bootstrapSwitch', function(event, state) {
                  var value = state?"1":"0"; 
                  var id = $(this).attr('data-id');
                  editAttribute(id,'published', value);
                });
        }
        });

        $('#page_table').on('click', 'span.edit-inline', function(){
            var id = $(this).attr('data-id'); 
            document.location.href = base_url + '/badmin/pages/edit/' + id;

        });


        /**delete table**/
        $('#page_table').on('click', '.delete-inline', function(e){
            var tr = $(this).closest('tr'); 
            var id = $(this).attr('data-id');
            BootstrapDialog.confirm('Are you sure delete this?', function(res){
                if(res){
                    $('#box_page').append(_loading);
                    $.ajax({
                        url:base_url + '/badmin/pages/' + id, 
                        method:'DELETE', 
                        success:function(res){
                            if(res){
                              tr.remove();
                              $('#ajax-loading').remove();  
                            } 
                        }
                    });
                }
            });
        });
    });
</script>


@endsection