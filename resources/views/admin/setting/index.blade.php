@extends('admin')
@section('content')
<?php 
    function getRecord($record, $value){
        if($record==null) return old($value);
        if(isset($record->$value)) return $record->$value;
        return "";
    }
?>

<section class="content-header">
  <h1>
        Settings
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/badmin') }}"><i class="fa fa-dashboard"></i>Home</a></li>
    <li class="active">Settings</li>
  </ol>
</section>

<section class="content">
    @include('partials._message')
    @include('partials._error')

    <div class="row">
        <form action="{{ url('badmin/settings') }}" method="post">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Site Settings</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">Site Name</label>
                            <input type="text" class="form-control" name="site_name" placeholder="Site Name" value="{{ getRecord($record,'site_name')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Site Tag line</label>
                            <input type="text" class="form-control" name="site_tag" placeholder="Site Tag line" value="{{ getRecord($record,'site_tag')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Site Title</label>
                            <input type="text" class="form-control" name="site_title" placeholder="Site Title" value="{{ getRecord($record,'site_title')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Site Description</label>
                            <textarea name="site_description" id="" rows="3" class="form-control">{{ getRecord($record,'site_description')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Site Footer</label>
                            <input type="text" class="form-control" name="site_footer" placeholder="Site Footer" value="{{ getRecord($record,'site_footer')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Google Web Master Verification Code</label>
                            <input type="text" class="form-control" name="gweb_master" placeholder="Google web master" value="{{ getRecord($record,'gweb_master')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Histat code</label>
                            <textarea name="histat_code" id="" rows="5" class="form-control">{{ getRecord($record,'histat_code')}}</textarea>
                        </div>
                    </div>

                    <div class="box-footer clearfix">
                      <div class="pull-right">
                        <button class="btn bg-olive btn-flat" name="submit" value="1">Save Changes</button>
                      </div>
                    </div
                </div>
            </div>
        </form>
    </div>
</section>


@endsection