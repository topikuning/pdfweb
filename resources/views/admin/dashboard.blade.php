@extends('admin')

@section('content')

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
             <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                   <span class="info-box-icon bg-green"><i class="fa fa-download"></i></span>
                   <div class="info-box-content">
                      <span class="info-box-text">Downloads</span>
                      <span class="info-box-number">{{ number_format($download,0,null,',') }}</span>
                   </div>
                   <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
             </div>
             <!-- /.col -->
             <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                   <span class="info-box-icon bg-red"><i class="fa fa-copy"></i></span>
                   <div class="info-box-content">
                      <span class="info-box-text">PDF Documents</span>
                      <span class="info-box-number">{{ number_format($countpdf,0,null,',') }}</span>
                   </div>
                   <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
             </div>
             <!-- /.col -->
             <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                   <span class="info-box-icon bg-aqua"><i class="fa fa-flag-o"></i></span>
                   <div class="info-box-content">
                      <span class="info-box-text">Published</span>
                      <span class="info-box-number">{{ number_format($countpublish,0,null,',') }}</span>
                   </div>
                   <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
             </div>
             <!-- /.col -->
             <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                   <span class="info-box-icon bg-yellow"><i class="fa fa-bookmark"></i></span>
                   <div class="info-box-content">
                      <span class="info-box-text">Pages</span>
                      <span class="info-box-number">{{ number_format($countpage,0,null,',') }}</span>
                   </div>
                   <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
             </div>
             <!-- /.col -->
          </div>
          <!-- end box -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-6 connectedSortable">


              <!-- Map box -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-box-tool" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->

                  <i class="fa fa-file-text"></i>
                  <h3 class="box-title">
                    Recent Pdf
                  </h3>
                </div>
                <div class="box-body">
                   <table class="table">
                     <thead>
                       <tr>
                         <th>Status</th>
                         <th>Title</th>
                         <th>Created at</th>
                       </tr>
                     </thead>
                     <tbody>
                      @foreach($recent_post as $post)
                       <tr>
                         <td>
                           @if($post->published)
                           <span class="label bg-green">Published</span>
                           @else
                           <span class="label bg-red">Draft</span>
                           @endif
                         </td>
                         <td>{{ $post->title }}</td>
                         <td>{{ $post->created_at }}</td>
                       </tr>
                       @endforeach
                     </tbody>
                   </table>
                </div><!-- /.box-body-->
                <div class="box-footer no-border">
                </div>
              </div>
              <!-- /.box -->

            </section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-6 connectedSortable">

              <!-- Map box -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-box-tool" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->

                  <i class="fa fa-file-text"></i>
                  <h3 class="box-title">
                    Recent Page
                  </h3>
                </div>
                <div class="box-body">
                   <table class="table">
                     <thead>
                       <tr>
                         <th>Status</th>
                         <th>Title</th>
                         <th>Created at</th>
                       </tr>
                     </thead>
                     <tbody>
                      @foreach($recent_page as $post)
                       <tr>
                         <td>
                           @if($post->published)
                           <span class="label bg-green">Published</span>
                           @else
                           <span class="label bg-red">Draft</span>
                           @endif
                         </td>
                         <td>{{ $post->title }}</td>
                         <td>{{ $post->created_at }}</td>
                       </tr>
                       @endforeach
                     </tbody>
                   </table>
                </div><!-- /.box-body-->
                <div class="box-footer no-border">
                </div>
              </div>
              <!-- /.box -->


            </section><!-- right col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ url('assets/dist/js/pages/dashboard.js') }}" type="text/javascript"></script>

@endsection