@extends('front')

@section('content')
        <hr class="topbar">
        <div class="jumbotron home-search" style="">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <br>
                        <p class="main_description">{{ $site_info->site_description }}</p>

                        <br><br>
                        <div class="row">

                            <div class="col-sm-8 col-sm-offset-2" style="text-align: center">
                                <div class="row">

                                    <div class="col-sm-10 col-sm-offset-1">
                                      <form action="{{ url('/search') }}" method="get">
                                        <div class="input-group">
                                            <span class="input-group-addon input-group-addon-text">Find pdf</span>
                                            <input type="text" name="q" class="form-control col-sm-3" placeholder="find a pdf document.. ">
                                            <div class="input-group-addon">
                                              <button type="submit" class="btn btn-primary">
                                                  Search
                                              </button>
                                            </div>

                                        </div>
                                      </form>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       @include('partials._widget_home')    
@endsection