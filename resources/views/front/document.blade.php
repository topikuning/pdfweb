@extends('front')

@section('content')

    @include('partials._sub_header')


<div class="container">
   <br>
   <div class="row">
      <div class="col-sm-12">
         <ol class="breadcrumb" itemprop="breadcrumb">
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="{{ url('/') }}" itemprop="url" title="{{ $site_info->site_title }} - {{ $site_info->site_description }} "><span itemprop="title">Home</span></a></li>
            <li class="" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="{{ url('result/'. $post->slug_keyword ) }}.html" itemprop="url"><span itemprop="title">{{ $post->keyword }}</span></a></li>
            <li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="{{ Request::url() }}?viewid={{ $post->id }}" itemprop="url"><strong itemprop="title">{{ $post->title }}</strong></a></li>
         </ol>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-3  hidden-xs">
         <div class="sidebar ">
            <div class="row ">
               <div class="col-sm-12">
                    @include('partials._recent_download')
                    @include('partials._recent_viewed')
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-9 pull-right listings" itemscope itemtype="http://schema.org/Article">
          <h1 itemprop="name headline">{{ $post->title }}</h1>
          <span>{{ $post->created_at->diffForHumans() }} <meta content="{{ $post->created_at->format('Y-m-d\TH:i:sP') }}" itemprop="datePublished"></span>
          <hr>
          <div class="row">
            <div class="col-md-12" itemprop="articleBody description">
              <p>{{ $post->keyword }} - {{ $post->abstract }}
                  <br>
                  <span>Downloaded : {{ $post->download }} </span>
              </p>
            </div>
          </div>
          <a href="{{ url('/download/'. $post->slug) }}.html" target="_blank" class="btn btn-primary download-wrap" rel="nofollow"><i class="fi-download"></i> Download</a>
          <div id="doc-viewer">
              <div id="loader_pdf">
                <div class="c100 p0" id="inner_loader">
                  <span>0%</span>
                  <div class="slice">
                    <div class="bar"></div>
                    <div class="fill"></div>
                  </div>
                </div>
               </div>
              <div id="pdf-viewer"></div>
              <div id="divcanvas">
                <a href="{{ url('/download/'. $post->slug) }}.html" target="_blank" class="btn btn-primary" rel="nofollow"><i class="fa fa-download"></i> Click here to download complete Ebook</a>
              </div>
          </div>
      </div>
   </div>
</div>


  <script type="text/javascript">
    $(document).ready(function(){
        function renderPDF(url, canvasContainer, options) {

            var options = options || { scale: 1 };

            var progressCallback = function(progress){
                var percentLoaded = progress.loaded / progress.total;
                var percent = Math.floor(percentLoaded * 100); 
                $('#inner_loader').attr('class','c100 p'+percent);
                $('#inner_loader span').text(percent + '%');
                if(percentLoaded ==1){
                    $('#loader_pdf').hide();
                    $('#divcanvas').show();
                    $('#doc-viewer').css({'overflow':'auto'});
                }
                
            };
                
            function renderPage(page) {

                var desiredWidth = $('#doc-viewer').width() - 20;
                if(desiredWidth < 550) desiredWidth =550;
                var viewport = page.getViewport(1);
                var scale = desiredWidth / viewport.width;
                var scaledViewport = page.getViewport(scale);
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                var renderContext = {
                  canvasContext: ctx,
                  viewport: viewport
                };

                canvas.height = scaledViewport.height;
                canvas.width = scaledViewport.width;

                $('#divcanvas').width(scaledViewport.width);

                canvasContainer.appendChild(canvas);
                
                page.render(renderContext);
            }
            
            function renderPages(pdfDoc) {
                var limit = 3; 
                if(pdfDoc.numPages < limit) limit = 2;
                for(var num = 1; num <= pdfDoc.numPages; num++){
                    if(num <= limit){
                         pdfDoc.getPage(num).then(renderPage);
                    }
                }


                   
            }

            PDFJS.disableWorker = true;
            PDFJS.getDocument(url,null,null,progressCallback).then(renderPages);

        }  
        var uri_doc = '{{ url("getpdf/".$post->slug) }}';
        renderPDF(uri_doc, document.getElementById('pdf-viewer'));

    });
  </script>

    @include('partials._widget_home') 
@endsection

