@extends('front')

@section('content')

@include('partials._sub_header')

<div class="container">
   <br>
   <div class="row">
      <div class="col-sm-12">
         <ol class="breadcrumb" itemprop="breadcrumb">
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="{{ url('/') }}" itemprop="url" title="{{ $site_info->site_title }} - {{ $site_info->site_description }} "><span itemprop="title">Home</span></a></li>
            <li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
               <a href="{{ Request::url() }}" itemprop="url">
                  <strong itemprop="title">{{ $records['total'] }} results for Sitemaps "{{ str_replace('-',' ',$records['slug']) }}"</strong>
               </a>
            </li>
         </ol>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-4  hidden-xs">
         <div class="sidebar ">
            <div class="row ">
               <div class="col-sm-12">
                    @include('partials._recent_download')
                    @include('partials._recent_viewed')
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-8 pull-right listings">
         <h1>Sitemaps {{ $current_title }}</h1>
         <ul>
         @foreach($records['data'] as $record)
          <li><a href="{{ url('/result/'. $record->slug .'.html') }}">{{ $record->title }}</a></li>
         @endforeach
         </ul>

         <div>
            <ul class="pagination pull-right">
               <?php $page = 1; $start=0; $limit= $records['limit']; ?>

               @while ($start < $records['total'])


               <?php
                  $url = url('/sitemaps/'.$records['slug']);
                  if($page > 1) $url = url('/sitemaps/'.$records['slug'].'/page/'. $page);    
               ?>

               <li><a href="{{ $url }}">{{ $page }}</a></li>
               <?php $start = $start + $limit;  $page++; ?>
               @endwhile
            </ul>
         </div>
      </div>
   </div>
</div>


    @include('partials._widget_home') 
@endsection