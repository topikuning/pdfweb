@extends('front')

@section('content')

@include('partials._sub_header')

<div class="container">
   <br>
   <div class="row">
      <div class="col-sm-12">
         <ol class="breadcrumb" itemprop="breadcrumb">
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="{{ url('/') }}" itemprop="url" title="{{ $site_info->site_title }} - {{ $site_info->site_description }} "><span itemprop="title">Home</span></a></li>
            <li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="{{ Request::url() }}" itemprop="url"><strong itemprop="title">{{ $keyword }}</strong></a></li>
         </ol>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-4  hidden-xs">
         <div class="sidebar ">
            <div class="row ">
               <div class="col-sm-12">
                    @include('partials._recent_download')
                    @include('partials._recent_viewed')
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-8 pull-right listings" itemscope itemtype="http://schema.org/ItemList">
        <h1 class="title" itemprop="name">{{ $keyword }}</h1>
        <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
           @foreach($records as $record)
           <div class="row premium listing-row" itemprop="item" itemscope="" itemtype="http://schema.org/Thing">
              <div class="ribbon-wrapper-red">
                 <div class="ribbon-red">&nbsp;<span>PDF FILE</span></div>
              </div>
              <div class="col-sm-12">
                 <h3 itemprop="name"><a class="" name="p{{ $record->id }}" href="{{ url('/result/'. $record->slug_keyword) }}.html?viewid={{ $record->id}}" rel="nofollow">{{ $record->title }}</a></h3>
                 <div class="muted">Posted {{ $record->created_at }}</div>
                 <p itemprop="description">{{ $record->abstract }}</p>
                 <p class="ad-description">
                    <strong>{{ $record->viewed }} views</strong> | 
                    <strong>{{ $record->download }} download</strong>             
                 </p>
                 <div>
                    <span class="classified_links">
                      <a class="link-info underline" href="{{ url('/result/'. $record->slug_keyword) }}.html?viewid={{ $record->id}}" itemprop="url" rel="nofollow">Download</a>
                    </span>
                 </div>
              </div>
            </div>
            @endforeach
          </div>
      </div>
   </div>
</div>


    @include('partials._widget_home') 
@endsection