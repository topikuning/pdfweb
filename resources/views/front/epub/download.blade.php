
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title>Download {{ $post->title }}.pdf</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="NOINDEX,NOFOLLOW,NOARCHIVE,NOODP,NOYDIR"/>


    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory-->
    <link rel="stylesheet" href="/epub/bootstrap.min.css">
    <link rel="stylesheet" href="/epub/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/epub/animate.css">
    <link rel="stylesheet" href="/epub/line-icons.css">
    <link rel="stylesheet" href="/epub/font-awesome.css">

    <!-- CSS Page Style -->
    <link rel="stylesheet" href="/epub/page_coming_soon.css">

</head>

<body class="coming-soon-page">

<div class="coming-soon-border"></div>
<div class="coming-soon-bg-cover"></div>


<!--=== Content Part ===-->
<div class="container cooming-soon-content">
    <!-- Coming Soon Content -->

    <div class="row">
        <div class="col-md-12 coming-soon">
            <h3>Download {{ $post->title }}.pdf soon before expire</h3>
            <p>Please wait 5 seconds, you will be redirected to the pdf download page.</p><br>
            <div style="padding-top:10px;">
                <a href="{{ url('/getpdf/'. $post->slug . '?download=true') }}" class="btn btn-primary" id="btndownload">Click here to download the PDF</a>
            </div>
        </div>
    </div>

    <!-- Coming Soon Plugn -->
    <div class="coming-soon-plugin">
        <div id="defaultCountdown"></div>
    </div>
</div><!--/container-->
<!--=== End Content Part ===-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="/epub/jquery.min.js"></script>
<script type="text/javascript" src="/epub/jquery-migrate.min.js"></script>
<script type="text/javascript" src="/epub/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="/epub/jquery.plugin.js"></script>
<script type="text/javascript" src="/epub/jquery.countdown.js"></script>
<script type="text/javascript" src="/epub/jquery.backstretch.min.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="/epub/app.js"></script>
<script type="text/javascript" src="/epub/page_coming_soon.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        PageComingSoon.initPageComingSoon();
    });
</script>

<!-- Background Slider (Backstretch) -->
<script>
    $.backstretch([
      "/epub/cover.jpg"
      ], {
        fade: 1000,
        duration: 4000
    });

    setTimeout(function(){
        var url = $('a#btndownload').attr('href');
        document.location.href = url;
    },5000);
</script>

<!--[if lt IE 9]>
    <script src="/epub/respond.js"></script>
    <script src="/epub/html5shiv.js"></script>
    <script src="/epub/placeholder-IE-fixes.js"></script>
<![endif]-->

{!! $histat_code !!}

</body>
</html>