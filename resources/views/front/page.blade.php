@extends('front')

@section('content')

<hr class="topbar">
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h1>{{ $page->title }}</h1>
            <hr>

            <div class="row">

                <div class="col-md-12">
                    {!! $page->body !!}
                </div>

            </div><!-- /row -->

        </div>

    </div>
</div>

@include('partials._widget_home') 

@endsection