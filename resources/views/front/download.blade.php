@extends('front')

@section('content')


    <hr class="topbar">
    <div class="container">
        <br>
       <div class="row">
          <div class="col-sm-12">
             <ol class="breadcrumb" itemprop="breadcrumb">
                <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="{{ url('/') }}" itemprop="url" title="{{ $site_info->site_title }} - {{ $site_info->site_description }} "><span itemprop="title">Home</span></a></li>
                <li class="" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="{{ url('result/'. $post->slug_keyword ) }}.html" itemprop="url"><span itemprop="title">{{ $post->keyword }}</span></a></li>
                <li class="active" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="{{ Request::url() }}" itemprop="url"><strong itemprop="title">{{ $post->title }}</strong></a></li>
             </ol>
          </div>
       </div>
        <div class="row">
            <div class="col-md-12" itemscope itemtype="http://schema.org/Article">
                <h2 class="text-center" itemprop="name headline">Download {{ $post->title }}</h2>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center" itemprop="articleBody description">Preparing the {{ $post->slug }}.pdf file to be downloaded. Please wait ...</h2>
                        <div id="counting_text" style="text-align:center">
                            <h2 class="text-center" id="counting_down">3</h2>
                        </div>
                    </div>

                </div><!-- /row -->

            </div>

        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            var count = 3; 
            var doc_url = '{{ url("/download/". $post->slug) }}';
            var url = '{{ url("/getpdf/". $post->slug . "?download=true") }}';
            var url_val = '{{ url("/download/validate/". $post->slug) }}';
            function setCount(){
                $('#counting_down').text(count + ' seconds');
                if(count > 0){
                    setTimeout(setCount,1000);
                }else{
                    $('#counting_text').html('<a class="btn btn-primary" href="'+ url+'" rel="nofollow">pdf file is ready. click here to DOWNLOAD IT</a>');
                    //document.location.href = url;

                }
                count--;
            }

            setCount();
        });
    </script>
    @include('partials._widget_home') 
@endsection