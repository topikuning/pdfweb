@extends('front')

@section('content')

@include('partials._sub_header')

<div class="container">
   <br>
   <div class="row">
      <div class="col-sm-12">
         <ol class="breadcrumb">
            <li class="active">{{ count($records) }} results for <strong>"{{ $squery }}"</strong></li>
         </ol>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-4  hidden-xs">
         <div class="sidebar ">
            <div class="row ">
               <div class="col-sm-12">
                    @include('partials._recent_download')
                    @include('partials._recent_viewed')
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-8 pull-right listings" itemscope itemtype="http://schema.org/ItemList">
        <h1 itemprop="name">Search with "{{ $squery }}"</h1>
        <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
           @foreach($records as $record)
           <div class="row premium listing-row" itemprop="item" itemscope="" itemtype="http://schema.org/Thing">
              <div class="ribbon-wrapper-red">
                 <div class="ribbon-red">&nbsp;<span>PDF FILE</span></div>
              </div>
              <div class="col-sm-12">
                 <h3 itemprop="name"><a class="" href="{{ url('/result/'. $record->slug_keyword) }}.html?viewid={{ $record->id }}" rel="nofollow">{{ $record->title }}</a></h3>
                 <p class="muted">Posted {{ $record->created_at }} to <a href="{{ url('/result/'. $record->slug_keyword) }}.html" class="underline">{{ $record->keyword }}</a></p>
                 <p itemprop="description">{{ $record->abstract }}</p>
                 <p class="ad-description">
                    <strong>{{ $record->viewed }} views</strong> | 
                    <strong>{{ $record->download }} download</strong>                
                 </p>
                 <p>
                    <span class="classified_links">
                      <a class="link-info underline" href="{{ url('/result/'. $record->slug_keyword) }}.html?viewid={{ $record->id }}" rel="nofollow" itemprop="url">Download</a>
                    </span>
                 </p>
              </div>
            </div>
            @endforeach
          </div>

      </div>
   </div>
</div>


    @include('partials._widget_home') 
@endsection