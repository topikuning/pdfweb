@extends('front')

@section('content')

    @include('partials._sub_header')

    <div class="sub-page">
        <div class="row">
          <div class="large-3 columns">
            @include('partials._recent_viewed')
            @include('partials._recent_download')
          </div>
          <div class="large-9 columns">
            <table width="100%" border="0">
              <tr>
                <td><strong>Ebook Title</strong></td>
                <td><strong>Abstract</strong></td>
              </tr>

              @foreach($records['data'] as $record)
              <tr>
                <td><a href="{{ url('/document/'. $record->slug) }}.html">{{ $record->title }}</a></td>
                <td>
                    {{ $record->abstract }}
                </td>
              </tr>

              @endforeach
            </table>
    
            <div>
            @if($records['page'] > 1)
                <a href="{{ url('/tag/'.$records['slug'].'/page/'. ($records['page'] - 1)) }}" class="button">Previous</a>
            @endif
            @if(($records['skip'] + $records['limit'])  < ($records['total']))
                <a href="{{ url('/tag/'.$records['slug'].'/page/'. ($records['page'] + 1)) }}" class="button">Next</a>
            @endif
            </div>
        </div>
      </div>
  </div>  

    @include('partials._widget_home') 
@endsection