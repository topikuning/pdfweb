        <nav class="navbar navbar-default" role="navigation">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a href="{{ url('/') }}" class="navbar-brand" title="{{ $site_info->site_name }}">
                        <span class="logo"><strong>{{ $site_info->site_name }}</strong><br />
                            <small >{{ $site_info->site_tag }} </small></span>
                    </a>

                </div>



                <div class="collapse navbar-collapse">
                    <div class="nav navbar-nav navbar-right hidden-xs">
                        <div class="row">
                            <div class="pull-right">
                                <a href="{{ url('register.html') }}">Register</a>
                            </div>  
                        </div>
                    </div>

                </div>
            </div>
        </nav>