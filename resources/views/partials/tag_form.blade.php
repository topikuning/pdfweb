<div class="row">
    <div class="col-md-12">
            <div class="box">
                <form action="" role="form">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">Keyword</label>
                            <input type="text" class="form-control" name="title" placeholder="Name" value="{{ $record->title }}">
                        </div>
                        <div class="form-group">
                            <label for="">Slug</label>
                            <input type="text" class="form-control" name="slug" placeholder="Slug" value="{{ $record->slug }}">
                        </div>
                        <div class="form-group">
                            <label>Created at</label>
                            <div class="input-group datepicker">
                              <input type="text" class="form-control" name="created_at" value="{{ $record->created_at }}">
                              <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
    </div>
</div>

<script type="text/javascript">
        $('.datepicker').datetimepicker({
            //format:'MM/DD/YYYY HH:mm:ss',
            format:'YYYY-MM-DD HH:mm:ss',
            defaultDate:moment()
        });
</script>