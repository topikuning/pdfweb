      <?php 

        if(!isset($navigation)){
          $navigation = [
            'parent' => 'dashboard',
            'child' => null
          ];
        }
      ?>
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ url('assets/images/mario.png') }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>{{ Auth::user()->name }}</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Site Content</li>
            <li class="{{ $navigation['parent']=='dashboard'?'active':'' }}"><a href="{{ url('/badmin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="{{ $navigation['parent']=='pages'?'active':'' }}"><a href="{{ url('/badmin/pages') }}"><i class="fa fa-file"></i> Pages</a></li>
            <li class="treeview {{ $navigation['parent']=='master'?'active':'' }}">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <!--<li class="{{ $navigation['child']=='categories'?'active':'' }}"><a href="{{ url('badmin/categories') }}"><i class="fa fa-circle-o"></i> Category</a></li> -->
                <li class="{{ $navigation['child']=='keywords'?'active':'' }}"><a href="{{ url('badmin/keywords') }}"><i class="fa fa-circle-o"></i> Keywords</a></li>
                <li class="{{ $navigation['child']=='post'?'active':'' }}"><a href="{{ url('badmin/posts') }}"><i class="fa fa-circle-o"></i> Post</a></li>
              </ul>
            </li>

            <li class="header">Settings</li>
            <li class="{{ $navigation['parent']=='settings'?'active':'' }}"><a href="{{ url('/badmin/settings') }}"><i class="fa fa-gear"></i> Site Setting</a></li>
            <li class="{{ $navigation['parent']=='profile'?'active':'' }}"><a href="{{ url('/badmin/profile') }}"><i class="fa fa-user"></i> User Profile</a></li>

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>