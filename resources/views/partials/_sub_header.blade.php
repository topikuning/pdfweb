
  <?php 
    $q = ""; 
    if(isset($squery)) $q = $squery;
  ?>

    <div class="jumbotron home-tron-search well ">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="home-tron-search-inner">
                    <div class="row">
                        <div class="col-sm-8 col-xs-9" style="text-align: center">
                            <div class="row">
                                <div class="col-sm-12 col-sm-offset-1">
                                    <form action="{{ url('/search') }}" method="get">
                                        <div class="input-group">
                                            <span class="input-group-addon input-group-addon-text hidden-xs">Find document</span>
                                            <input type="text" class="form-control col-sm-3" placeholder="find pdf document.. " name="q" value="{{ $q }}">
                                            <div class=" input-group-addon hidden-xs">
                                                <div class="btn-group">
                                                    <button type="submit" class="btn">
                                                    Go
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>