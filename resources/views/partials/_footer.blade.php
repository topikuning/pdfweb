<div class="footer">
    <div class="container">

        <div class="row">

            <div class="col-sm-4 col-xs-12">
                <p><strong>{{ $site_footer }}</strong></p>
                <p>All rights reserved</p>
            </div>          

            <div class="col-sm-8 col-xs-12">
                <p class="footer-links">
                    @foreach($pages as $page)
                        <a href="{{ url('pages/'. $page->slug) }}">{{ $page->title }}</a> 
                    @endforeach
                </p>
            </div>
        </div>
    </div>
</div>

{!! $histat_code !!}


