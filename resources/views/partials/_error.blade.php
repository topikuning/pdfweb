    @if (count($errors) > 0)
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-danger">
              <h4>Warning!</h4>
              <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
              </ul>
            </div>           
        </div>
    </div>
    @endif