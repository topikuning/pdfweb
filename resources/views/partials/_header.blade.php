  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {!! $site_info->gweb_master !!}
    @if(!isset($current_title))
        <title itemprop="name">{{ $site_info->site_title }}</title>
    @else
        <title itemprop="name">{{ $current_title }} | {{ $site_info->site_title }}</title>
    @endif

    @if(!isset($no_description))
        @if(!isset($current_description))
            <meta name="description" itemprop="description" content="{{ $site_info->site_description }}" />
        @else
            <meta name="description" itemprop="description" content="{{ $current_description }}"  />
        @endif
    @endif

    @if(isset($noindex))
        <meta name="robots" content="noindex,follow" />
    @endif

    @if(isset($nofollow))
         <meta name="robots" content="noindex,nofollow" />
    @endif
    
    <!-- Bootstrap core CSS -->
    <link id="switch_style" href="{{ url('bootlisting/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ url('bootlisting/css/theme.css') }}" rel="stylesheet">
    <link href="{{ url('bootlisting/css/dropzone.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ url('css/circle.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/custom.css') }}">

    <script src="{{ url('/bootlisting/js/jquery-1.10.2.min.js') }}"></script>
    <script src="{{ url('/bootlisting/bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ url('/bootlisting/js/jquery.flot.js') }}"></script>
    <script src="{{ url('/bootlisting/js/global.js') }}"></script>
    @if(isset($pdfjs))
        <script src="{{ url('js/pdf.js') }}"></script>
    @endif
<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":"/pages/privacy-policy","theme":"dark-bottom"};
</script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<!-- End Cookie Consent plugin -->
  </head>