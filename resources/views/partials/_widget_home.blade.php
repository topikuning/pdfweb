  <div class="container">



    <div class="row">

        <div class="col-md-12">

            <div class="row directory">
                <div class="col-sm-12 ">
                    <h2><span>TOP LIST DOCUMENTS</span></h2>
                </div>
            </div>

            <div class="row directory">


                <div class="col-xs-12">
                      <div class="directory-block col-sm-3 col-xs-5">
                        <div class="row">
                            <div class="col-sm-3">
                                <i class="fa fa-tags"></i>
                            </div>
                            <div class="col-sm-9">
                                <h4>Top Keywords</h4>
                                   <ul>
                                   @foreach($top_qr as $rec)
                                    <li><a href="{{ url('/result/'. $rec->slug) }}.html" title="{{ $rec->title }}">{{ $rec->title }}</a></li>
                                  @endforeach
                                  </ul> 
                            </div>
                        </div>
                      </div>

                      <div class="directory-block col-sm-3 col-xs-5">
                        <div class="row">
                            <div class="col-sm-3">
                                <i class="fa fa-tags"></i>
                            </div>
                            <div class="col-sm-9">
                                <h4>Recent Keywords</h4>
                                <ul>
                                @foreach($recent_qr as $rec)
                                <li><a href="{{ url('/result/'. $rec->slug) }}.html" title="{{ $rec->title }}">{{ $rec->title }}</a></li>
                                @endforeach 
                                </ul>
                            </div>
                        </div>
                      </div>

                      <div class="directory-block col-sm-3 col-xs-5">
                        <div class="row">
                            <div class="col-sm-3">
                                <i class="fa fa-tags"></i>
                            </div>
                            <div class="col-sm-9">
                                <h4>Popular Documents</h4>
                                <ul>
                                @foreach($popular_doc as $rec)
                                <li><a href="{{ url('/result/'. $rec->slug_keyword) }}.html#p{{ $rec->id }}" title="{{ $rec->title }}">{{ str_limit($rec->title,22) }}</a></li>
                               @endforeach 
                                </ul>
                            </div>
                        </div>
                      </div>

                      <div class="directory-block col-sm-3 col-xs-5">
                        <div class="row">
                            <div class="col-sm-3">
                                <i class="fa fa-tags"></i>
                            </div>
                            <div class="col-sm-9">
                                <h4>Recent Documents</h4>
                                <ul>
                                @foreach($recent_doc as $rec)
                                <li><a href="{{ url('/result/'. $rec->slug_keyword) }}.html#p{{ $rec->id }}" title="{{ $rec->title }}">{{ str_limit($rec->title,22) }}</a></li>
                                @endforeach 
                                </ul> 
                            </div>
                        </div>
                      </div>


               </div>


            </div>




        </div>
    </div>    

    <div class="row">
        <div class="col-md-12">
            <div class="row directory">
                <div class="col-sm-12 ">
                    <h2><span>SITEMAP</span></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $alpha = ['1','2','3','4','5','6','7','8','9', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']; ?>
                    @foreach($alpha as $ch)
                        <span style="padding-right:10px;"><strong><a rel="" href="{{ url('/sitemaps/'.$ch) }}">{{ $ch }}</a></strong></span>
                    @endforeach
                </div>
            </div>
        </div>
    </div>    

</div>