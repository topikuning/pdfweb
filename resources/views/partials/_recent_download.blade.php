
          <div class="panel panel-default">
             <div class="panel-heading">Recent Downloaded</div>
                <ul class="list-group">
                    @foreach($rec_download as $rec)
                    <li class="list-group-item"><a href="{{ url('/result/' . $rec->slug) }}.html"><i class="fi-checkbox"></i> {{ $rec->title }}</a></li>
                    @endforeach
                </ul>
          </div>