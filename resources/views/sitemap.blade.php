{!! '<?xml version="1.0" encoding="UTF-8" ?>' !!}
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{ config('site.site_url') }}</loc>
        <priority>1</priority>
        <changefreq>always</changefreq>
    </url>
    @foreach($pages as $page)
    <url>
        <loc>{{ config('site.site_url').'/pages/'. $page->slug }}</loc>
        <priority>0.5</priority>
        <changefreq>monthly</changefreq>
    </url>
    @endforeach
</urlset>