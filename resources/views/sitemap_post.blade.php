{!! '<?xml version="1.0" encoding="UTF-8" ?>' !!}
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($posts as $post)
    <?php
        $lastmod = new DateTime($post->created_at); 
        $datemode = $lastmod->format('Y-m-d\TH:i:sP'); 
    ?>
    <url>
        <loc>{{ url('/document/'.  $post->slug) }}.html</loc>
        <priority>0.8</priority>
        <lastmod>{{ $datemode }}</lastmod>
        <changefreq>daily</changefreq>
    </url>
    @endforeach
</urlset>