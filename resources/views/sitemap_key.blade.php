{!! '<?xml version="1.0" encoding="UTF-8"?>' !!}
 {!! style() !!}
    <urlset xmlns:xsi="http://www.w3.org/201/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        <url>
            <loc>{{ url('/') }}</loc>
            <lastmod>{{ $lastmod }}</lastmod>
            <changefreq>hourly</changefreq>
            <priority>1</priority>
        </url>
        @foreach($posts as $post)
        <url>
            <loc>{{ url('/result/'.  $post->slug) }}.html</loc>
            <lastmod>{{ lastModFormat($post->created_at) }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
        @endforeach
    </urlset>
    <?php
        function style(){
            return '<?xml-stylesheet type="text/xsl" href="'.url('/sitemap.xsl').'"?>';
        }
        function lastModFormat($date){
           $lastmod = new DateTime($date); 
           return $lastmod->format('Y-m-d\TH:i:sP'); 
        } 
    ?>