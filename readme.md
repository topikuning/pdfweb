## Simple PDF Ebook

-- minimum system requirement 
- php 5.4 up 
- memcached php extension. 
- mysql database 
- composer jika mau install manual

-- install pdftk server

-- install nodejs

--yandex with nodejs 
$ npm install -g forever
$ npm install -g phantomjs
$ git clone git://github.com/n1k0/casperjs.git  
$ cd casperjs
$ ln -sf `pwd`/bin/casperjs /usr/local/bin/casperjs

-- chmod -R o+w storage/
-- run "composer install" di root folder
-- salin .env.example file  dan taruh root folder dengan nama .env
-- setting .env 
    DB_HOST=host_anda
    DB_DATABASE==db_anda
    DB_USERNAME=user_anda
    DB_PASSWORD=password anda 



-- jalankan di terminal "php artisan migrate" untuk generate table-table yang diperlukan

-- jika tidak ingin menggunakan memcache untu cache driver silahkan ganti 
   (tidak direkomendasikan karena akan menurunkan performa speed page load)
    CACHE_DRIVER=file
    SESSION_DRIVER=file
    QUEUE_DRIVER=database

-- mengaktifkan registrasi admin user. defaultnya false. untuk mengaktifkan set di file .env bagian 
  ADMIN_REGISTER=true

  buka dibrowser yourweb.com/auth/register
  pastikan nonaktif lagi setelah anda membuat user untuk admin 
  ADMIN_REGISTER=false

  untuk mengakses panel admin 
  yourweb.com/badmin


  upload query daemon 
php artisan queue:work --daemon &


