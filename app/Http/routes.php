<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function() use ($app) {

    $app->get('/', 'HomeController@index');
    $app->get('/sitemap.xml', 'SiteMapController@index');
    //$app->get('/sitemap/sitemap-main.xml.gz','SiteMapController@main');
    $app->get('/sitemap/sitemap-key-{id}.xml','SiteMapController@keyword');
    //$app->get('/sitemap/sitemap-posts-{id}.xml.gz','SiteMapController@posts');
    $app->get('/search', 'ResultController@search');
    $app->get('/pages/{slug}', 'HomeController@page');

    $app->get('/sitemaps/{alpha}','ResultController@sitemaps');
    $app->get('/sitemaps/{alpha}/page/{page}','ResultController@sitemaps');

    $app->get('/result/{slug}.html', 'ResultController@getResult');
    //$app->get('/result/{slug}/page/{page}', 'ResultController@result');

    //$app->get('/tag/{slug}', 'TagController@getResult');
    //$app->get('/tag/{slug}/page/{page}', 'TagController@result');


    //$app->get('/document/{slug}.html', 'ResultController@document');

    $app->get('register.html', 'HomeController@register'); 
    $app->post('register.html', 'HomeController@register');

    $app->get('/download/{slug}.html','DownloadController@index');
    //$app->post('/download/validate/{slug}', 'DownloadController@setSession');
    $app->get('/getpdf/{slug}','DownloadController@getSourcePdf');
   
    $app->get('/auth/login', 'AuthController@getLogin');
    $app->post('/auth/login', 'AuthController@postLogin');
    $app->get('/auth/logout', 'AuthController@getLogout');

    $app->get('auth/register', 'AuthController@getRegister');
    $app->post('auth/register', 'AuthController@postRegister');


});


$app->group(['prefix' => 'badmin', 'middleware' => 'auth.admin', 'namespace' => 'App\Http\Controllers\Admin'], function() use ($app){
    $app->get('/createSlug', 'TagController@createSlug');
    $app->get('/','DashBoardController@index');

    $app->get('/keywords','TagController@index');
    $app->get('/keywords/read','TagController@read');
    $app->get('/keywords/create','TagController@getCreate');
    $app->post('/keywords/create','TagController@postCreate');
    $app->delete('/keywords/{id}', 'TagController@destroy');
    $app->get('/keywords/edit/{id}', 'TagController@getEdit');
    $app->post('/keywords/edit/{id}', 'TagController@postEdit');
    $app->get('/keywords/upload', 'TagController@upload');
    $app->post('/keywords/upload', 'TagController@postUpload');

    $app->get('/categories', 'CategoriesController@index');
    $app->post('/categories/create', 'CategoriesController@postCreate');
    $app->post('/categories/createnode', 'CategoriesController@postNodeCreate');
    $app->post('/categories/edit/{id}', 'CategoriesController@postEdit');
    $app->get('/categories/tree', 'CategoriesController@getTreeList');
    $app->delete('/categories/destroy/{id}', 'CategoriesController@destroy');

    $app->get('/posts', 'PostController@index');
    $app->get('/posts/read', 'PostController@read');
    $app->get('/posts/edit/{id}', 'PostController@edit');
    $app->get('/posts/create', 'PostController@create');
    $app->post('/posts/create', 'PostController@postCreate');
    $app->post('/posts/attribute','PostController@postEditAtrribute');
    $app->delete('/posts/{id}', 'PostController@destroy');
    $app->get('/posts/download/{file}', 'PostController@download');


    $app->get('/pages','PageController@index');
    $app->get('/pages/create','PageController@create');
    $app->post('/pages/create','PageController@store');
    $app->get('/pages/read', 'PageController@read');
    $app->get('/pages/edit/{id}', 'PageController@edit');
    $app->delete('/pages/{id}', 'PageController@destroy');
    $app->post('/pages/attribute','PageController@postEditAtrribute');

    $app->get('/settings','SettingController@index'); 
    $app->post('/settings','SettingController@store');

    $app->get('/profile','UserController@getProfile'); 
    $app->post('/profile','UserController@postProfile'); 

});