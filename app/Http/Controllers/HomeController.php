<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller {

    public function index()
    {
        return view('front.home');
    }

    public function page($slug)
    {
        $page = DB::table('pages')->where('slug', $slug)->first();
        if(!$page) abort(404);

        $current_title = $page->title;
        return view('front.page',['page' => $page, 'current_title' => $current_title, 'no_description' => true, 'noindex' => true]);
    }

    public function register()
    {
        return view('front.register',[
            'current_title' => 'Register Page', 
            'current_description' => 'Please register to get more Free Ebook'
        ]);
    }
}