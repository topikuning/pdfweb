<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\FuncCollection;
use Storage;
use Session;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;

class DownloadController extends Controller {

    public function index($slug)
    {
        $post = DB::table('posts')->where('slug', $slug)->where('published','1')->first();
        if(!$post) return redirect('/');

        $agent = new Agent();
        $isrobot = $agent->isRobot();
        
        $data = [
            'post' => $post,
            'current_title' => 'Download Ebook '. $post->title,
            'nofollow' => true,
            'current_description' =>'Download '. $post->keyword .' - '. $post->title,
        ];

        if($isrobot){
            $data['isrobot'] = true;
        }

        return view('front.epub.download',$data);

    }


    public function setSession($slug)
    {
        $post = DB::table('posts')->where('slug', $slug)->where('published','1')->first();
        if(!$post) return "false"; 
        Session::put($post->id .'_download', 'true');
        return "ok";

    }


    public function getHtml($slug)
    {
        $post = DB::table('posts')->where('slug', $slug)->where('published','1')->first();
        if(!$post) return redirect('/');
        return view('front.document_html',['post' => $post]);
    }

    public function getSourcePdf($slug, Request $request)
    {
        
        
        $post = DB::table('posts')->where('slug', $slug)->where('published','1')->first();
        if(!$post) return ""; 
        $filename = $post->pdf_file; 
        $this->curlStream($filename,$post, $request);

    }



    private function curlStream($url,$post, $request)
    {
        ignore_user_abort(true);
        set_time_limit(0);
        //define("DOMPDF_ENABLE_AUTOLOAD", false);


        $tmpfile = base_path() .'/storage/app/'.str_random(8) .'.tmp';
        $fp = fopen ($tmpfile, 'w+');//This is the file where we save the    information
        $ch = curl_init($url);//Here is the file we are downloading, replace spaces with %20
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch); // get curl response
        curl_close($ch);
        fclose($fp);

        $created = Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at);
        $last_modified = $created->format('D, d M Y H:i:s \G\M\T');
        $expires = $created->addMonth()->format('D, d M Y H:i:s \G\M\T');

        if($post->engine=='wordbing'){

            $ext = FuncCollection::getFileExtension($tmpfile);
            

            $rawtext = ($ext=='doc') || ($ext=='docx'); 
            if($rawtext){
                require_once base_path(). '/vendor/dompdf/dompdf/dompdf_config.inc.php';
                if($ext=='doc') $text = FuncCollection::getRawWordText($tmpfile); 
                if($ext=='docx') $text = FuncCollection::read_docx($tmpfile); 
                unlink($tmpfile);
                $dompdf = new DOMPDF();
                $dompdf->load_html($text);
                $dompdf->render();
                //$dompdf->stream($post->slug.'.pdf');
                file_put_contents($tmpfile, $dompdf->output());
            }

            if(strtolower($ext)=='odt'){
                
                if(strtolower($ext)=='odt'){
                    $phpWord = \PhpOffice\PhpWord\IOFactory::load($tmpfile,'ODText'); 
                }else{
                    $phpWord = \PhpOffice\PhpWord\IOFactory::load($tmpfile); 
                }
                
                $domPdfPath = base_path() . '/vendor/dompdf/dompdf';
                $rendererName = \PhpOffice\PhpWord\Settings::PDF_RENDERER_DOMPDF;
                \PhpOffice\PhpWord\Settings::setPdfRenderer($rendererName, $domPdfPath);
                $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'PDF');
                $wtmp = base_path() .'/storage/app/'.str_random(8) .'.pdf';
                $objWriter->save($wtmp);
                unlink($tmpfile); 
                $tmpfile = $wtmp;
            }


        }

        if(!$request->has('download')){
            $newfile = base_path() . '/storage/app/'. str_random(5).'.pdf'; 
            $cmd = 'pdftk '.$tmpfile .' cat 1-3 output '. $newfile . ' compress 2>&1'; 
            $run = shell_exec($cmd); 
            if(str_contains($run, 'Errors')){
                $cmd = 'pdftk '.$tmpfile .' cat 1 output '. $newfile . ' compress'; 
                $run = shell_exec($cmd); 
            }

            unlink($tmpfile); 
            $tmpfile = $newfile;

            /**
            require_once base_path(). '/vendor/dompdf/dompdf/dompdf_config.inc.php';
            $linkfile =  base_path() . '/storage/app/'. str_random(5).'.pdf'; 
            $html = '<h1 style="align:center;"><a href="'.url('document/'.$post->slug.'.html').'">Download completed ebook here</a></h1>';
            $dompdf = new DOMPDF();
            $dompdf->load_html($html);
            $dompdf->render();
            file_put_contents($linkfile, $dompdf->output());
            $newfile =  base_path() . '/storage/app/'. str_random(5).'.pdf'; 
            $cmd = 'pdftk '.$tmpfile.' '.$linkfile.' cat output '.$newfile;
            shell_exec($cmd); 
            unlink($linkfile); 
            unlink($tmpfile);
            $tmpfile = $newfile;
            **/

        }

        if($request->has('download')){
           FuncCollection::insertDownload($post);
           $agent = new Agent();
           $isrobot = $agent->isRobot();

           if(!$isrobot){
                //you can go with redirect adcenter here
           }
        }
    


        $size = filesize($tmpfile);
        header('Content-Type: application/pdf'); 
        header('Content-Disposition: inline; filename="'.$post->slug.'.pdf"');
        header("Content-type: application/octet-stream");
        header('Content-length: '. $size);
        header('Last-Modified: '. $last_modified); 
        header('Expires: '. $expires); 
        header('Cache-Control: public');
        $this->readfile_chunked($tmpfile);
        unlink($tmpfile);


    }

      public function readfile_chunked($filename, $retbytes = TRUE) {
        $buffer = "";
        $cnt =0;
        $handle = fopen($filename, "rb");
        if ($handle === false) {
          return false;
        }
        while (!feof($handle)) {
          $buffer = fread($handle, 1024*1024);
          echo $buffer;
          ob_flush();
          flush();
          if ($retbytes) {
            $cnt += strlen($buffer);
          }
        }
        $status = fclose($handle);
        if ($retbytes && $status) {
          return $cnt; // return num. bytes delivered like readfile() does.
        }
        return $status;
      }

}
