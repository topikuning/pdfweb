<?php namespace App\Http\Controllers;

use Request;
use DB;
use App\Models\FuncCollection;
use Carbon\Carbon;
use App\Models\GeneratePost;

class ResultController extends Controller {

    private function getPost($page,$limit,$q, $keyword=false, $sitemap=false){

        if($keyword){
            $post = DB::table('posts')
                    ->where('published', '1')
                    ->where('slug_keyword',$q)
                    ->orderBy('title','abstract');
        }else{
            /**
            $post = DB::table('posts')
                    ->where('published', '1')
                    ->where('created_at', '<=',DB::raw('now()'))
                    ->where(function($query) use($q) {
                        $query->where('title', 'like', '%'. $q . '%')
                            ->orWhere('keyword', 'like', '%'. $q . '%')
                            ->orWhere('abstract', 'like', '%'. $q . '%');
                    })->orderBy('title','abstract');  
            **/
            if(!$sitemap){
            $post = DB::table('posts')
                    ->whereRaw('MATCH(title,keyword,abstract) AGAINST(? IN BOOLEAN MODE)',[$q])
                    ->orderBy('title')
                    ->take(30)
                    ->get(); 
            return $post; 
            }


        }

        if($sitemap){
            $post = DB::table('recent_querys')
                 ->where('title','like', $q .'%')
                 ->where('created_at', '<=',DB::raw('now()'))
                 ->orderBy('title');

        }




        $total = $post->count();

        if($page < 1) $page = 1;

        $skip = ($page * $limit ) - $limit;

        if($skip > $total){
          $page = 1;
          $skip = 0;
        }

        $data  = $post->skip($skip)
                ->take($limit)
                ->get();

        $titles = []; 

        foreach($data as $row){
            $titles[]  = $row->title;
        }

        return [
            'total' => $total, 'data' => $data, 
            'page' => $page, 'limit' => $limit,
            'titles' => $titles,
            'skip' => $skip,
            'slug' => $q
        ];

    }

    public function getResult($slug)
    {
        return $this->result($slug,1);
    }

    public function search()
    {

        $q = Request::input('q'); 
        $page = Request::input('page')?Request::input('page'):'1';
        $q = trim($q);
        if(!$q)  $q = '';
        $data = []; 
        $data['current_title'] = $q;
        $data['current_description'] = 'Search ebooks with keyword '. $q;
        $data['squery'] = $q;
        $data['records'] = $this->getPost($page,15,$q);
        //FuncCollection::insertQuery($data['records']['data']);
        return view('front.search', $data);
    }

    public function result($slug)
    {

        $keyword = DB::table('recent_querys')
        ->where('created_at', '<=',DB::raw('now()'))
        ->where('slug', $slug)->first(); 
        if(!$keyword) abort(404); 

        $obj = new GeneratePost; 
        $obj->updatePost($keyword);


        $viewid = Request::input('viewid'); 
        if($viewid){
            if(is_numeric($viewid)){
                return $this->document($viewid);
            }
        }

        $records = DB::table('posts')
                   ->select('*')
                   ->where('published','1')
                   ->where('post_id', $keyword->id)
                   ->get(); 


        $current_description = []; 
        foreach ($records as $key => $value) {
            $current_description[] = $value->title;
        }

        $current_description = $keyword->title .' - '. implode('. ', $current_description); 
        $meta_description = $keyword->meta_description; 
        if($meta_description) {
            //$current_description = $keyword->title .' - '. $meta_description;
        }


        $data = [
            'current_title' => $keyword->title,
            'current_description' => $current_description,
            'keyword' => $keyword->title,
            'records' => $records
        ];
        return view('front.result', $data);
    }


    public function document($slug)
    {

        $post = DB::table('posts')->where('id', $slug)->where('published','1')->first();
        if(!$post) abort(404);
        $data =array();
        FuncCollection::insertViewed($post);
        $post->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at);
        $post->download = FuncCollection::formatDownload($post->download);
        $data['current_title'] = $post->title;
        $data['noindex'] = true;
        $data['current_description'] = $post->keyword .' - '.$post->abstract ;
        $data['post'] = $post;
        $data['pdfjs'] = true;

        return view('front.document', $data);


    }

    private function getTags($id)
    {
        $data = DB::table('post_tag')
                    ->join('tags','post_tag.tag_id','=', 'tags.id')
                    ->where('post_tag.post_id',$id)
                    ->select('tags.name','tags.slug')
                    ->get();
        return $data;
    }

    public function sitemaps($alpha, $page=1)
    {
        $length =strlen($alpha);
        if($length != 1) abort(404);
        $qtitle = $alpha;
        $q = $alpha;
        $data = []; 
        $title_page = ""; 
        if($page > 1) $title_page = ' page '. $page;
        $data['current_title'] = $qtitle .' | Alphabets' . $title_page;
        $data['records'] = $this->getPost($page,50,$q,false, true);
        $data['no_description'] = true;
        $data['noindex'] = true;
        return view('front.sitemaps', $data);


    }



}