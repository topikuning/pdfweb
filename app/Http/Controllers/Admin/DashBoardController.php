<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;


class DashBoardController extends Controller {

    public function index(){
        $data = [
            'recent_post' => Post::getLatest(5),
            'recent_page' => Post::getLatestPage(5),
            'download'    => Post::getDownload(),
            'countpdf'    => Post::getCount(),
            'countpublish' => Post::getPublishCount(),
            'countpage' => Post::getPageCount()
        ];
        return view('admin.dashboard', $data);
    }
}
