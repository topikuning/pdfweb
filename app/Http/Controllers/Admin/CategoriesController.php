<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FuncCollection;
use DB;
use stdClass;



class CategoriesController extends Controller {


    /**
     * [createSlug description]
     * @param  [type] $text [description]
     * @return [type]       [description]
     */
    public function createSlug(Request $request)
    {
        $text = $request->input('text');
        if(trim($text)){
            return str_slug($text);
        }

        return "";
    }

    /**
     * [createListCategories description]
     * @return string [description]
     */
    private function createListCategories(){
        $rows = DB::table('categories')->orderBy('parent_id','ASC')->get();
        $treeObj = new FuncCollection;
        $treeObj->buildTree($rows);
        $tree = $treeObj->stringTree;
        $tree = str_replace('<ul></ul>', '', $tree);
        return $tree;

    }

    /**
     * [getTreeList description]
     * @return [type] [description]
     */
    public function getTreeList()
    {
         $tree = $this->createListCategories();
         $data = ['cattree' => $tree]; 
         return view('partials._treecat', $data);
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        $tree = $this->createListCategories();
        $navigation = ['parent' => 'master', 'child' => 'categories'];
        $data = ['navigation' => $navigation, 'cattree' => $tree];
        return view('admin.cat', $data);
    }

    /**
     * [postCreate description]
     * @param  Request $request [description]
     * @param  boolean $node    [description]
     * @return [type]           [description]
     */
    public function postCreate(Request $request, $node=false)
    {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required|unique:categories'
        ]);

        $data = $request->all(); 
        $data['slug'] = str_slug($data['slug']); 
        $data['created_at'] = DB::raw('now()');
        DB::table('categories')->insert($data); 
        $cat = DB::table('categories')->orderBy('id','DESC')->first(); 
        if($node) return $cat;
        return json_encode($cat);

    }

    /**
     * [postNodeCreate description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postNodeCreate(Request $request){
        $element = $this->postCreate($request, true);
        return '<li data-id="'. $element->id.'" data-slug="'.$element->slug.'"><span class="text-label">'. $element->name .'</span>'. FuncCollection::buildTool(). '</li>';

    }

    /**
     * [postEdit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function postEdit(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            //'slug' => 'required|unique:tags'
        ]);


        $data = [
            'name' => $request->input('name'),
            'updated_at' => DB::raw('now()')
        ]; 

        DB::table('categories')->where('id',$id)->update($data);

        return json_encode(DB::table('categories')->where('id',$id)->first());               
    }

    /**
     * [nestedDelete description]
     * @param  [type] $record [description]
     * @return [type]         [description]
     */
    private function nestedDelete($record) 
    {
        $data = DB::table('categories')->where('parent_id', $record->id)->get(); 
        foreach($data as $item ){
            $this->nestedDelete($item);
        }
        DB::table('categories')->where('id', $record->id)->delete();
    }

    /**
     * [destroy description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {        
        $obj = new stdClass(); 
        $obj->id = $id;

        $this->nestedDelete($obj);

        return "1";
    }


}
