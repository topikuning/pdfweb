<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FuncCollection;
use DB;
use Validator;
use stdClass;
use Session;
use Auth;


class UserController extends Controller {
    private function setNavigation()
    {
        $navigation = ['parent' => 'profile', 'child' => ''];
        $data = ['navigation' => $navigation];
        return $data;
    }

    public function getProfile()
    {
        $data = $this->setNavigation();
        $data['record'] = Auth::user();

        return view('admin.setting.profile', $data);
    }

    public function postProfile(Request $request)
    {


        $v = Validator::make($request->all(),[
                'name' => 'required',
                'password' => 'confirmed|min:6'
            ]);


        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput($request->except('password'));
        }

        $user = Auth::user();

        $data = ['name' => $request->input('name'), 'updated_at' => DB::raw('now()')]; 
        if(trim($request->input('password'))){
            $data['password']  = bcrypt($request->input('password')); 
        }

        DB::table('users')->where('id', $user->id)->update($data);
        Session::flash('message','Profile updated'); 
        return redirect()->back();


    }
}