<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FuncCollection;
use DB;
use Validator;
use stdClass;
use Session;


class SettingController extends Controller {
    private function setNavigation()
    {
        $navigation = ['parent' => 'settings', 'child' => ''];
        $data = ['navigation' => $navigation];
        return $data;
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        $data = $this->setNavigation();
        $record = null;
        $data = DB::table('settings')->get();
        if($data) $record = new stdClass;
        foreach($data as $rec){
            $name = $rec->name;
            $record->$name = $rec->value;
        }
        $data['record'] = $record;
        return view('admin.setting.index', $data);
    }

    public function store(Request $request){
        $v = Validator::make($request->all(),[
                'site_name' => 'required',
                'site_title' => 'required'
            ]);


        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput($request->all());
        }

        $data = $request->all();
        unset($data['submit']);
        $count = DB::table('settings')->count();
        if(!$count){
            $this->insert($data);
        }else{
            $this->update($data);
        }
        Session::flash('message','Settings updated'); 
        return redirect()->back();

    }

    private function insert($data)
    {
        $insert = [];
        foreach($data as $key => $value){
            $insert[] =[
                'name' => $key,
                'value' => $value,
                'created_at' => DB::raw('now()')
            ]; 
        }

        DB::table('settings')->insert($insert);
    }

    private function update($data)
    {
        foreach ($data as $key => $value) {
            DB::table('settings')->where('name', $key)->update(
                ['value' => $value,'updated_at' => DB::raw('now()')]);
        }
    }
}