<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FuncCollection;
use DB;
use Validator;
use stdClass;
use Session;
use Storage;


class PostController extends Controller {


    private function setNavigation()
    {
        $navigation = ['parent' => 'master', 'child' => 'post'];
        $data = ['navigation' => $navigation];
        return $data;
    }


    private function getAdditionalData($data)
    {
        $data['tag_list'] = DB::table('tags')->select('id','name')->get();
        $rows = DB::table('categories')->get();
        $func = new FuncCollection;
        $func->buildCheckBoxTree($rows);
        $cats = $func->stringTree;
        $data['cats'] = $cats;
        return $data;
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        $data = $this->setNavigation();
        return view('admin.post.index', $data);
    }

    /**
     * [read description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function read(Request $request)
    {
        $draw = $request->input('draw');
        $start = $request->input('start'); 
        $length = $request->input('length'); 
        $order = $request->input('order');
        $search = $request->input('search');


        $columns = [
            'id',
            'title',
            'author_name',
            'abstract',
            'viewed',
            'download',
            'published',
            'created_at',
            'pdf_file'
        ];




        $orderField = $columns[$order[0]['column']]; 
        $oderDirect = $order[0]['dir']; 

        $table = DB::table('posts')->select(
                'id',
                'title',
                'author_name',
                'abstract',
                'viewed',
                'download',
                'published',
                'created_at',
                'pdf_file')->orderBy($orderField,$oderDirect); 

        if(trim($search['value'])){
            $index =0;
            foreach($columns as $column){
                if(!$index){
                    $table = $table->where($column, 'LIKE', '%'.  $search['value'] .'%');
                }else{
                    $table = $table->orWhere($column, 'LIKE', '%'.  $search['value'] .'%');
                }

                $index++;
            }
        }

        $total_data = $table->count();

        $data = $table->skip($start)->take($length)->get();
        $arr = []; 
        foreach ($data as $key => $value) {
            $tmp = [];
            foreach($value as $item){
                $tmp[] = $item;
            }
            $tmp[] = '<a href="'.url('/badmin/posts/edit/'.$value->id).'"><span class="badge bg-green edit-inline" data-id="'.$value->id.'">Edit</span></a>'.
                     '<span class="badge bg-red delete-inline" data-id="'.$value->id.'">Delete</span>';
            $arr[] = $tmp;
        }


        return [
            'draw' => $draw,
            'recordsTotal' => $total_data,
            'recordsFiltered' => $total_data,
            'data' => $arr
        ];
    }


    /**
     * [create description]
     * @return [type] [description]
     */
    public function create()
    {
        $data = $this->setNavigation();
        //$data = $this->getAdditionalData($data);
        $record = new stdClass;
        $record->id = '';
        $record->title = ''; 
        $record->slug = ''; 
        $record->author_name = '';
        $record->author_avatar = ''; 
        $record->abstract = '';
        $record->body = '';
        $record->created_at = ''; 
        $record->pdf_file = '';
        $record->cat = []; 
        $record->tag = [];
        $data['record'] = $record;
        return view('admin.post.create', $data);
    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($id)
    {
        $record = DB::table('posts')->where('id',$id)->first();
        if(!$record) return redirect('/badmin/posts');
        //$record->cat = FuncCollection::getCats($id);
        $record->cat = [];
        $record->tag = FuncCollection::getTags($id);
        $data = $this->setNavigation();
        //$data = $this->getAdditionalData($data);
        $data['record'] = $record;
        return view('admin.post.create', $data);

    }

    /**
     * [saveCategories description]
     * @param  [type] $post_id [description]
     * @param  [type] $cats    [description]
     * @return [type]          [description]
     */
    private function saveCategories($post_id, $cats){
        DB::table('cat_post')->where('post_id', $post_id)->delete(); 
        foreach ($cats as $key => $value) {
            DB::table('cat_post')->insert([
                'post_id' => $post_id, 
                'category_id' => $value
            ]);
        }
    }


    /**
     * [saveTags description]
     * @param  [type] $post_id [description]
     * @param  [type] $tags    [description]
     * @return [type]          [description]
     */
    private function saveTags($post_id, $tags){
        DB::table('post_tag')->where('post_id', $post_id)->delete(); 
        foreach ($tags as $key => $value) {
            DB::table('post_tag')->insert([
                'post_id' => $post_id, 
                'tag_id' => $value
            ]);
        }
    }


    /**
     * [postCreate description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreate(Request $request)
    {
        $filter = [
            'title' => 'required',
            //'author_name' => 'required',
            'author_avatar' => 'url',
            'abstract' => 'required',
            //'created_at' => 'required|date_format:m/d/Y H:i:s',
            'created_at' => 'required|date_format:Y-m-d H:i:s'
        ];

        if(!$request->input('id'))
            $filter['slug'] = 'required|unique:posts';

        $r = $request->all();
        $has_file = false;

        if($request->hasFile('pdf_file')){
            $has_file = true;
            $r['pdf_file'] = $request->file('pdf_file');
            $filter['pdf_file'] =  'mimes:pdf';
        }

        $v = Validator::make($r, $filter);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput($request->all());
        }

        $insert = [
            'title' => $request->input('title'),
            'author_name' => $request->input('author_name'),
            'author_avatar' => $request->input('author_avatar'),
            'abstract' => $request->input('abstract'),
            'body' => $request->input('body'),
            'created_at' => $request->input('created_at'),
            'published' => $request->input('submit')?'1':'0'
        ];

        if($has_file){
            $file_name = str_slug($request->input('title')) . '.pdf';
            $insert['pdf_file'] = $file_name;
            $request->file('pdf_file')->move(storage_path('app'), $file_name);
        }
        if($request->input('id')){
            $id = $request->input('id'); 
            $insert['updated_at'] = DB::raw('now()');
            DB::table('posts')->where('id', $id)->update($insert);
            $message = 'Edit Post success';

        }else{
            $insert['slug'] = str_slug($request->input('slug'));
            $id = DB::table('posts')->insertGetId($insert);
            $message = 'New Post was created';
        }
        
        /**
        $cats = $request->input('cats'); 
        if($cats == null) $cats = []; 

        $tags = $request->input('tags'); 
        if($tags == null) $tags = []; 

        //$this->saveCategories($id,$cats); 
        $this->saveTags($id,$tags);
        **/
        Session::flash('message',$message);
        if($request->input('id')){
            return redirect()->back();   
        }
        return redirect('/badmin/posts');
    }

    /**
     * [destroy description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
        $ok = DB::table('posts')->where('id', $id)->delete(); 
        //$this->saveCategories($id,[]); 
        $this->saveTags($id,[]);
        return $ok;
    }

    /**
     * [postEditAtrribute description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function postEditAtrribute(Request $request)
    {
        $id = $request->input('id'); 
        $field = $request->input('field'); 
        $value = $request->input('value'); 
        $ok = DB::table('posts')->where('id',$id)->update(
            [$field => $value]
        ); 

        return $ok;
    }

    /**
     * [download description]
     * @param  [type] $file [description]
     * @return [type]       [description]
     */
    public function download($file)
    {
        if (Storage::exists($file)) {
            $content = Storage::get($file);
            return response($content, 200)
              ->header('Content-Disposition', 'attachment; filename='. $file)
              ->header('Content-Type', 'application/pdf');
        }

    }


}
