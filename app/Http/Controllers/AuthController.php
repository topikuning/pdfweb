<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;

class AuthController extends Controller {

    public function __construct(){
         $this->middleware('auth.guest',['except' => 'getLogout']);
    }

    public function getLogout(){
        Auth::logout();
        return redirect('/auth/login');
    }

    public function getLogin(){
        return view('auth/login');
    }

    public function postLogin(Request $request){

         $v = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput($request->except('password'));
        }

        $email = $request->input('email'); 
        $password = $request->input('password'); 
        $remember = $request->input('remember'); 

        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
            return redirect('/badmin');
        }else{
            return redirect()->back()->withErrors(['Invalid login'])->withInput($request->except('password'));
        }

    }

    public function getRegister(){
        if(env('ADMIN_REGISTER','false') != 'true') return redirect('/');
        return view('auth/register');
    }


    public function postRegister(Request $request, User $user){
         if(env('ADMIN_REGISTER','false') != 'true') return redirect('/');
         $v = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'name' => 'required|min:3',
            'password' => 'required|min:6'
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput($request->except('password'));
        } 

        $input = $request->all(); 

        $newuser = $user->create($input);

        return redirect('auth/login');  
    }
}