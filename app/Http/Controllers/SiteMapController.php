<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;
use Carbon\Carbon;

class SiteMapController extends Controller {
    
    public function index()
    {
        $now = Carbon::now();

        $post = DB::table('recent_querys')->where('created_at', '<=', DB::raw('now()'))->orderBy('created_at','DESC')->first();
        if($post){
            $now = Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at);
        }

        ob_get_clean();
        header('Content-Type: text/xml;charset=utf-8'); 
        echo '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.url('/sitemap-index.xsl').'"?>';
        echo '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">';
        set_time_limit(0);

        $page = 1; 
        $limit = 200; 
        $is_done = false;
        $data =  DB::table('recent_querys')->where('created_at', '<=', DB::raw('now()'))->count();  

        $subtract = $data / $limit;
        $now = $now->subMinutes(intval($subtract));            

        while (!$is_done) {
           $offset = $page * $limit;
           $fakeoffset = $offset;
           $offset = $offset -1;
           if($page ==1) $offset = '0'; 
            $url = url('sitemap/sitemap-key-'. $page . '.xml'); 
            $datemod = $now->format('Y-m-d\TH:i:sP');
            echo '<sitemap><loc>'.$url.'</loc><lastmod>'.$datemod.'</lastmod></sitemap>';
            flush();         
            if($fakeoffset >= $data) $is_done = true; 
            $now->addMinute();
            $page++;
             
         } 

        echo '</sitemapindex>';


    }

    public function main()
    {
        $name = 'sitemap-main.xml.gz';
        $exist = Storage::exists($name);

        if($exist){
            $content = Storage::get($name); 
            $content = gzdecode($content);
            $new_url = url('/');
            $content = str_replace(config('site.site_url'), $new_url, $content);
            return response(gzencode($content))
                        ->header('Content-Encoding','UTF-8')
                        ->header('Content-type','application/x-gzip; charset=UTF-8')
                        ->header('Content-Disposition','attachment; filename="'. $name .'"');

       }

        return "";
    }



    public function posts($page)
    {
        $name = 'sitemap-posts-'.$page.'.xml.gz';
        $page = is_numeric($page)?$page:1; 
        $limit = 10000;
        $offset = ($page-1) * $limit;

        $data = DB::table('posts')
                ->select('*')
                ->where('published','1')
                ->orderBy('id')
                ->skip($offset)
                ->take($limit)
                ->get();
        $content = view('sitemap_post',[
          'posts' => $data,
        ])->render();



        return response(gzencode("\xEF\xBB\xBF".$content))
                        ->header('Content-Encoding','UTF-8')
                        ->header('Content-type','application/x-gzip; charset=UTF-8')
                        ->header('Content-Disposition','attachment; filename="'. $name .'"');


        
    }

  public function keyword($page)
    {

        $last = DB::table('recent_querys')->where('created_at', '<=', DB::raw('now()'))->orderBy('created_at','DESC')->first();
        if(!$last) return ""; 
        $lastmod = Carbon::createFromFormat('Y-m-d H:i:s', $last->created_at);


        $page = is_numeric($page)?$page:1; 
        $limit = 200;
        $offset = ($page-1) * $limit;

        $sql = "
            SELECT recent_querys.* 
            FROM   recent_querys 
            JOIN (SELECT id 
                  FROM   recent_querys 
                  WHERE created_at <= NOW()
                  ORDER  BY created_at 
                  LIMIT ".$offset.", ".$limit.") AS t ON t.id = recent_querys.id
        ";

        $data = DB::select($sql);

        /**
        $data = DB::table('recent_querys')
                ->select('title','slug as slug_keyword', 'created_at')
                ->where('created_at', '<=', DB::raw('now()'))
                ->orderBy('created_at','ASC')
                ->skip($offset)
                ->take($limit)
                ->get();
        **/


        $content = view('sitemap_key',[
          'lastmod' => $lastmod->format('Y-m-d\TH:i:sP'),
          'posts' => $data
        ])->render();


        return response($content)
                        ->header('Content-Encoding','UTF-8')
                        ->header('Content-type','text/xml;charset=utf-8');

        
    }


}