<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\FuncCollection;

class TagController extends Controller {

    private function getTag($page,$limit,$slug){


        $post = DB::table('tags')
                ->where('tags.slug', $slug)
                ->join('post_tag', 'tags.id', '=', 'post_tag.tag_id')
                ->join('posts', 'post_tag.post_id', '=', 'posts.id')
                ->select('posts.*')
                ->orderBy('posts.created_at','DESC');

        $total = $post->count();

        if($page < 1) $page = 1;

        $skip = ($page * $limit ) - $limit;

        if($skip > $total){
          $page = 1;
          $skip = 0;
        }

        $data  = $post->skip($skip)
                ->take($limit)
                ->get();

        return [
            'total' => $total, 'data' => $data, 
            'page' => $page, 'limit' => $limit,
            'skip' => $skip,
            'slug' => $slug
        ];

    }

    public function result($slug,$page)
    {
        if(!is_numeric($page)) $page = 1;
        $q = str_replace('-', ' ', $slug); 
        $data = []; 
        $data['current_title'] = 'Download ebooks taggable with '. $q;
        $data['squery'] = $q;
        $data['records'] = $this->getTag($page,10,$slug);
        return view('front.result_tag', $data);
    }


    public function getResult($slug)
    {
        return $this->result($slug,1);
    }



}