<?php namespace App\Composers;


use DB;
use stdClass;

class ComposerNavigation {


    private function getSiteInfo()
    {
       
        $record = new stdClass; 
        $record->site_name = ""; 
        $record->site_title = ""; 
        $record->site_tag = ""; 
        $record->site_footer = ""; 
        $record->site_description = ""; 
        $record->gweb_master=""; 
        $record->histat_code="";
        $data =  DB::table('settings')->get();

        foreach ($data as $key => $value) {
            $name = $value->name; 
            $record->$name = $value->value;
        }

        return $record;
    }

    /**
     * get random data from post fastest as possible
     * @return [type] [description]
     */
    private function getFastestRandom()
    {
        $sql = "SELECT *
              FROM recent_querys AS r1 JOIN
                   (SELECT CEIL(RAND() *
                                 (SELECT MAX(id)
                                    FROM recent_querys)) AS id)
                    AS r2
             WHERE r1.id >= r2.id AND r1.created_at <= NOW()
             ORDER BY r1.id ASC
             LIMIT 10";

        return DB::select($sql);
    }


    public function setFooter($view)
    {
        $pages = DB::table('pages')->where('published','1')->orderBy('title')->get();
        $site_info = $this->getSiteInfo(); 
        $site_footer  = $site_info->site_footer;
        $data = [
            'pages' => $pages,
            'site_footer' => $site_footer,
            'histat_code' => $site_info->histat_code
        ];
        $view->with($data);
    }

    public function setHeader($view){
        $site_info = $this->getSiteInfo(); 
        $view->with('site_info', $site_info);
    }


    public function setWidgetHome($view){
        $recent_doc = DB::table('posts')
                    ->where('published','1')
                    ->where('created_at', '<=', DB::raw('now()'))
                    ->orderBy('created_at','DESC')
                    ->take(10)
                    ->get();


        $popular_doc = DB::table('posts')
                       ->where('created_at', '<=', DB::raw('now()'))
                       ->orderBy('viewed', 'DESC')
                       ->take(10)
                       ->get();

        $recent_qr = DB::table('recent_querys')
                    ->where('created_at', '<=', DB::raw('now()'))
                    ->orderBy('created_at','DESC')
                    //->groupBy('slug')
                    ->take(10)
                    ->get();

        $top_qr = DB::table('recent_querys')
                  ->where('created_at', '<=', DB::raw('now()'))
                  ->orderBy('title', 'ASC')
                  ->take(10)
                  ->get();
        /**DB::table('recent_querys')
                       ->groupBy('slug')
                       ->take(5)
                       ->get();
        **/

        $view->with([
            'recent_doc'  => $recent_doc,
            'popular_doc' => $popular_doc,
            'recent_qr'   => $recent_qr,
            'top_qr'      => $top_qr
        ]);

    }


    public function setWidgetRecentView($view)
    {
        $rec_viewed = DB::table('recent_viewed')
                    ->orderBy('created_at','DESC')
                    ->groupBy('slug')
                    ->take(5)
                    ->get();

        $view->with('rec_viewed', $rec_viewed);
    }

    public function setWidgetRecentDownload($view)
    {
        $rec_download = DB::table('recent_download')
                    ->orderBy('created_at','DESC')
                    ->groupBy('slug')
                    ->take(5)
                    ->get();

        $view->with('rec_download', $rec_download);
    }

}

