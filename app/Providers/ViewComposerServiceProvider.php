<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{

    /**
     * [boot description]
     * @return [type] [description]
     */
    public function boot()
    {
        $this->composeNavigation();
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
 
    }

    private function composeNavigation()
    {
        view()->composer(['partials._footer', 'front.epub.download'], 'App\Composers\ComposerNavigation@setFooter');
        view()->composer(['partials._header','front.*', 'partials._sub_header', 'partials._frontnav'], 'App\Composers\ComposerNavigation@setHeader');
        view()->composer('partials._widget_home', 'App\Composers\ComposerNavigation@setWidgetHome');
        view()->composer('partials._recent_viewed', 'App\Composers\ComposerNavigation@setWidgetRecentView');
        view()->composer('partials._recent_download', 'App\Composers\ComposerNavigation@setWidgetRecentDownload');
    }
}
