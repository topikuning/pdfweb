<?php namespace App\Models;

use DB; 

class Post {
    
    public static function getLatest($limit)
    {
        return DB::table('posts')->orderBy('created_at','DESC')->take($limit)->get();
    }

    public static function getLatestPage($limit)
    {
        return DB::table('pages')->orderBy('created_at','DESC')->take($limit)->get();
    }

    public static function getDownload()
    {
        return  DB::table('posts')->sum('download');
    }

    public static function getCount()
    {
        return DB::table('posts')->count();
    }

    public static function getPublishCount()
    {
        return DB::table('posts')->where('published','1')->count();
    }

    public static function getPageCount()
    {
        return DB::table('pages')->count();
    }
}