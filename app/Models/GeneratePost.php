<?php namespace App\Models; 

use Carbon\Carbon;
use DB; 
use Storage;


class GeneratePost {

private function utf8_uri_encode( $utf8_string, $length = 0 ) {
  $unicode = '';
  $values = array();
  $num_octets = 1;
  $unicode_length = 0;

  $string_length = strlen( $utf8_string );
  for ($i = 0; $i < $string_length; $i++ ) {

    $value = ord( $utf8_string[ $i ] );

    if ( $value < 128 ) {
      if ( $length && ( $unicode_length >= $length ) )
        break;
      $unicode .= chr($value);
      $unicode_length++;
    } else {
      if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

      $values[] = $value;

      if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
        break;
      if ( count( $values ) == $num_octets ) {
        if ($num_octets == 3) {
          $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
          $unicode_length += 9;
        } else {
          $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
          $unicode_length += 6;
        }

        $values = array();
        $num_octets = 1;
      }
    }
  }

  return $unicode;
}

private function seems_utf8($str) {
  $length = strlen($str);
  for ($i=0; $i < $length; $i++) {
    $c = ord($str[$i]);
    if ($c < 0x80) $n = 0; # 0bbbbbbb
    elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
    elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
    elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
    elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
    elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
    else return false; # Does not match any model
    for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
      if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
        return false;
    }
  }
  return true;
}

private function url_title($title, $separator = '-', $capitalize = FALSE)
{
  $title = strip_tags($title);
  $title = str_replace(array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6"), '', $title);
  // Preserve escaped octets.
  $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
  // Remove percent signs that are not part of an octet.
  $title = str_replace('%', '', $title);
  // Restore octets.
  $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

  if ($this->seems_utf8($title)) {
    if (function_exists('mb_strtolower')) {
      $title = mb_strtolower($title, 'UTF-8');
    }
    $title = $this->utf8_uri_encode($title, 200);
  }

  $title = preg_replace('/\`\~\!\@\#\$\%\^\&\*\(\)\_\+\-\=\<\>\?\:\"\{\}\|\,\.\/\;\[\]/', '', $title);
  $title = str_replace('.', $separator, $title);
  $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
  $title = ($capitalize) ? safe_ucwords($title): $title;
  $title = preg_replace('/\s+/', $separator, $title);
  $title = preg_replace('|-+|', $separator, $title);
  $title = rtrim($title, $separator);
  $title = trim($title);
  $title = stripslashes($title);
  $title = ($capitalize) ? $title: $this->safe_strtolower($title);
  $title = urldecode($title);
  //$title = html_entity_decode($title, ENT_QUOTES, "UTF-8");
  return $title;
}

private function permalink_url ($str, $replace_separator_to_space = false)
{
  $str = $this->url_title($str, '-', false);
  
  if ($replace_separator_to_space)
  {
    $str = str_replace(config('separator'), ' ', $str);
  }

  return $str;
}

private function safe_string ($str)
{
  $str = $this->url_title($str, '-');
  $str = str_replace('-', ' ', $str);
  return $str;
}

private function safe_ucfirst($str)
{
  if ($this->seems_utf8($str))
  {
    if (function_exists('mb_strtoupper'))
    {
      $encoding = "UTF-8";
      $strlen = mb_strlen($str, $encoding);
      $first_char = mb_substr($str, 0, 1, $encoding);
      $then = mb_substr($str, 1, $strlen - 1, $encoding);
      $str = mb_strtoupper($first_char, $encoding) . $then;
    }
  }
  else
  {
    $str = ucfirst($str);
  }

  return $str;
}

private function safe_strtolower($str)
{
  if ($this->seems_utf8($str))
  {
    if (function_exists('mb_convert_case'))
    {
      $str = mb_convert_case($str, MB_CASE_LOWER, "UTF-8");
    }
  }
  else
  {
    $str = strtolower($str);
  }

  return $str;
}

private function safe_ucwords($str)
{
  if ($this->seems_utf8($str))
  {
    if (function_exists('mb_convert_case'))
    {
      $str = mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
    }
  }
  else
  {
    $str = ucwords($str);
  }

  return $str;
}

private function safe_string_insert ($str, $type)
{
  $str = $this->title_case($this->safe_string($str));

  switch ($type) {
    case 'title':
      if (strlen($str) < 3)
      {
        $str = "Untitled Document";
      }
      else if (strlen($str) > 3 && strlen($str) < 8)
      {
        $str = "Document " . $str;
      }
      break;
    case 'desc':
      if (strlen($str) < 3)
      {
        $str = "No Description";
      }
      else if (strlen($str) > 3 && strlen($str) < 8)
      {
        $str = "Document " . $str;
      }
      break;
  }

  return $str;
}

private function bersihkata($kata)
{
  $kata = strip_tags($kata);
  $kata = $this->safe_strtolower($kata);
  $kata = $this->clean_words($kata);
  $kata = $this->permalink_url($kata, ' ');
  $kata = $this->safe_ucwords($kata);
  return $kata;
}

private function title_case ($title)
{
  $regx = '/<(code|var)[^>]*>.*?<\/\1>|<[^>]+>|&\S+;/';
  preg_match_all($regx, $title, $html, PREG_OFFSET_CAPTURE);
  $title = preg_replace ($regx, '', $title);
  $q_left = chr(8216);
  $q_right = chr(8217);
  $double_q = chr(8220);

  preg_match_all ('/[\w\p{L}&`\''. $q_left . $q_right .'"'. $double_q .'\.@:\/\{\(\[<>_]+-? */u', $title, $m1, PREG_OFFSET_CAPTURE);
  foreach ($m1[0] as &$m2) {
    list ($m, $i) = $m2;
    $i = mb_strlen (substr ($title, 0, $i), 'UTF-8');
    
    $m = $i>0 && mb_substr ($title, max (0, $i-2), 1, 'UTF-8') !== ':' && 
      !preg_match ('/[\x{2014}\x{2013}] ?/u', mb_substr ($title, max (0, $i-2), 2, 'UTF-8')) && 
       preg_match ('/^(a(nd?|s|t)?|b(ut|y)|en|for|i[fn]|o[fnr]|t(he|o)|vs?\.?|via)[ \-]/i', $m)
    ? mb_strtolower ($m, 'UTF-8')
    : ( preg_match ('/[\'"_{(\['. $q_left . $double_q .']/u', mb_substr ($title, max (0, $i-1), 3, 'UTF-8'))
    ? mb_substr ($m, 0, 1, 'UTF-8').
      mb_strtoupper (mb_substr ($m, 1, 1, 'UTF-8'), 'UTF-8').
      mb_substr ($m, 2, mb_strlen ($m, 'UTF-8')-2, 'UTF-8')
    : ( preg_match ('/[\])}]/', mb_substr ($title, max (0, $i-1), 3, 'UTF-8')) ||
      preg_match ('/[A-Z]+|&|\w+[._]\w+/u', mb_substr ($m, 1, mb_strlen ($m, 'UTF-8')-1, 'UTF-8'))
    ? $m
    : mb_strtoupper (mb_substr ($m, 0, 1, 'UTF-8'), 'UTF-8').
      mb_substr ($m, 1, mb_strlen ($m, 'UTF-8'), 'UTF-8')
    ));
    
    $title = mb_substr ($title, 0, $i, 'UTF-8').$m. mb_substr ($title, $i+mb_strlen ($m, 'UTF-8'), mb_strlen ($title, 'UTF-8'), 'UTF-8');
  }

  foreach ($html[0] as &$tag) $title = substr_replace ($title, $tag[0], $tag[1], 0);
  return $title;
}


  private function badWord($str)
  {
    $bad = "death,dead,deceased,demise,die,dying,expire,fatal stroke,past away,perished,cocaine,drugs,heroin,marijuana,medication,morphine,overdose,oxycodone,oxycontin,pharmaceutical,pharmacy,shit,piss,fuck,tits,drown,drowned,electrocuted,electrocution,execution,killed,killer,manslaughter,miscarriage,murder,murdered,poisoned,poisoning,slaughtered,strangler,strangulation,suffocation,suicide,abortion,aneurysm,bum,crash,cancer,cerebral accident,desanguinated,disfigured,embolism,hemorrhage,horror,maimed,paralyzed,stroke,erection,masturbation,pedophile,penis,porn,pussy,squirting,pussy,schlong,sex,xx,xxx,xxxx,squirting,squirt,blowjob,public sex,p0rn,memek,ngentot,itil,kontol,burial,casket,funeral,attack,bomb,bomber,incerated,jail,prison,terrorist,casino,gambling,google,las vegas,video poker,poker,chip poker,ass,asshole,anal,creampie,bukkake,rapid,rapidshare,megaupload,hotfile";
    $arr = explode(',', $bad); 

    $result = false; 
    foreach($arr as $find){
        if(strpos(strtolower($str), $find)) $result = true;
    }

    return $result;

  }


  public function updatePost($post){
    if($post->scrap=='0'){
        $this->generatePost($post);
        DB::table('recent_querys')->where('id', $post->id)->update(['scrap' => '1']);
    }
  }

  private function generatePost($post){
    $engine = config('site.engine_scrap');
    $keyword = $post->title; 
    $url = config('site.node_server_url') . '/output.json?site='. $engine .'&keyword='. urlencode($keyword);
      $ch = curl_init($url);
      curl_setopt_array(
        $ch,
        array(
           CURLOPT_SSL_VERIFYPEER => false,
           CURLOPT_RETURNTRANSFER  => true
        )
      );
      $content = curl_exec($ch);
      curl_close($ch);
      if($content){
        $data = json_decode($content); 
        $arr = [];
        foreach($data as $row){
            if($row->title){
              $en_title = $this->safe_string_insert($row->title, 'title');
              $en_desc = $this->safe_string_insert($row->abstract, 'desc');
              $keyword = $this->safe_string_insert($keyword, 'title');
                $arr[] =[
                  'title' => $en_title,
                  'abstract' => $en_desc,
                  'body' => $en_desc,
                  'viewed' => rand(0,1000),
                  'download' => rand(0,1000),
                  'keyword' => $keyword,
                  'slug_keyword' => str_slug($keyword),
                  'engine' => $engine,
                  'pdf_file' => $row->pdf_file
                ];
            }

        }

        if(count($arr)){
            shuffle($arr); 
            foreach($arr as $row){
                if(!$this->badWord($row['title'])){
                    $row['slug'] = str_slug($row['title']); 
                    $row['created_at'] = $post->created_at; 
                    $row['post_id'] = $post->id;
                    $found = DB::table('posts')->where('slug', $row['slug'])->first(); 
                    if(!$found){
                        DB::table('posts')->insert($row);
                    }

                }
            }
        }

      }



   }

}