<?php namespace App\Models;

use DB; 

class User {


    public function create($input){
        $user = DB::table('users')->insert([
            'email' => $input['email'],
            'name' => $input['name'],
            'created_at' => DB::raw('now()'),
            'password' => bcrypt($input['password'])
        ]);

        return $user;

    }
}