<?php namespace App\Models; 

use DB;

class FuncCollection {

    public $stringTree = "";
    

    /** 
      * Return URL-Friendly string slug
      * @param string $string 
      * @return string 
      */

    public static function seoUrl($string) {
        return str_slug($string);
    }

    public static function  buildTool(){
        $toolTree  = '<span class="btn btn-box-tool inline-edit"><i class="fa fa-edit" data-toggle="tooltip" data-original-title="Edit"></i></span>';
        $toolTree  .= '<span class="btn btn-box-tool inline-delete"><i class="fa fa-trash-o" data-toggle="tooltip" data-original-title="Delete"></i></span>';
        $toolTree  .= '<span class="btn btn-box-tool inline-add"><i class="fa fa-plus" data-toggle="tooltip" data-original-title="Add Child"></i></span>';
        return $toolTree;
    }

    /**
     * [buildTree description]
     * @param  array   $elements [description]
     * @param  integer $parentId [description]
     * @return [type]            [description]
     */
     public function buildTree(array $elements, $parentId = 0) 
     {
        $branch = array();
        $this->stringTree .='<ul>';

        foreach ($elements as $element) {
            if ($element->parent_id == $parentId) {
                $this->stringTree .= '<li data-id="'. $element->id.'" data-slug="'.$element->slug.'"><span class="text-label">'. $element->name .'</span>'. $this::buildTool(). '</li>';
                $children = $this->buildTree($elements, $element->id);
                if ($children) {
                    $element->children = $children;
                }
                $branch[] = $element;
            }
        }
        $this->stringTree .='</ul>';
        return $branch;
    } 

    /**
     * [buildCheckBoxTree description]
     * @param  array   $elements [description]
     * @param  integer $parentId [description]
     * @return [type]            [description]
     */
     public function buildCheckBoxTree(array $elements, $parentId = 0) 
     {
        $branch = array();
        $this->stringTree .='<ul style="list-style:none;">';

        foreach ($elements as $element) {
            if ($element->parent_id == $parentId) {
                $this->stringTree .= '<li><div class="checkbox"><input type="checkbox" name="cats[]" value="'.$element->id.'">'. $element->name .'</div></li>';
                $children = $this->buildCheckBoxTree($elements, $element->id);
                if ($children) {
                    $element->children = $children;
                }
                $branch[] = $element;
            }
        }
        $this->stringTree .='</ul>';
        return $branch;
    } 

    /**
     * [phpdateFormat description]
     * @param  [type] $string [description]
     * @return [type]         [description]
     */
    public static function phpdateFormat($string)
    {
        if(!$string) return $string;
        $tmp = explode(' ', $string); 
        $date = explode('/', $tmp[0]); 

        return $date[2] .'-'. $date[0] .'-' . $date[1] . ' ' . $tmp[1];
    }


    public static function getTags($post_id)
    {
        $data = []; 
        $rows = DB::table('post_tag')->where('post_id', $post_id)->get();
        foreach ($rows as $key => $value) {
            $data[] = $value->tag_id;
        }

        return $data;

    }

    public static function getCats($post_id)
    {
        $data = []; 
        $rows = DB::table('cat_post')->where('post_id', $post_id)->get();
        foreach ($rows as $key => $value) {
            $data[] = $value->category_id;
        }

        return $data;

    }

    /**
     * [truncateStringWords description]
     * @param  [type] $str    [description]
     * @param  [type] $maxlen [description]
     * @return [type]         [description]
     */
    public static function truncateStringWords($str, $maxlen)
    {
        if (strlen($str) <= $maxlen) return $str;

        $newstr = substr($str, 0, $maxlen);
        if (substr($newstr, -1, 1) != ' ') $newstr = substr($newstr, 0, strrpos($newstr, " "));

        return $newstr;
    }

    public static function formatDate($str)
    {
       $time = strtotime($str);
       return date("d M Y g:i A", $time);     
    }

    public static function formatDownload($number)
    {
        if(!$number) return "-"; 
        $format_number = number_format($number,0,null,',');
        if($number > 1) return $format_number . ' copies'; 
        return $format_number . ' copy';
    }

    public static function insertViewed($post){
        DB::table('recent_viewed')->insert([
            'title' => $post->keyword,
            'slug' => $post->slug_keyword, 
            'created_at' => DB::raw('now()')
        ]);
        $viewed = [
          'viewed' => $post->viewed + 1
        ];
        //DB::table('posts')->where('id',$post->id)->update($viewed);
    }

    public static function insertDownload($post){
        DB::table('recent_download')->insert([
            'title' => $post->keyword,
            'slug' => $post->slug_keyword, 
            'created_at' => DB::raw('now()')
        ]);
        $download = [
          'download' => $post->download + 1
        ];
        DB::table('posts')->where('id',$post->id)->update($download);
    }

    public static function getKeySlug($string)
    {
        $string = str_slug($string); 
        $string = str_replace('-', ' ', $string);
        $string = str_ireplace('or', '', $string);
        $string = str_ireplace('and', '', $string);
        $arr = explode(' ', $string); 
        if(count($arr) < 4) return $string;
        return $arr[0]. ' ' . $arr[1] .' '. $arr[2];

    }

    public static function insertQuery($posts)
    {
        $index =1; 
        $insert = [];
        foreach($posts as $post){
            if($index < 3){
                $title = $post->title;
                $slug =  $post->keyword; 
                $insert[] = [
                    'title' => $slug,
                    'slug' => str_slug($slug),
                    'created_at' => DB::raw('now()')
                ];

            }
            $index++;
        }

        if(count($insert)) DB::table('recent_querys')->insert($insert);        

    }

    public static function getFileExtension($file){
        $ext = 'pdf'; 
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $file);
        finfo_close($finfo);
        if($mime=='application/vnd.openxmlformats-officedocument.wordprocessingml.document') $ext = 'docx'; 
        if($mime=='application/msword') $ext = 'doc'; 
        if($mime=='application/vnd.oasis.opendocument.text') $ext = 'odt';
        return $ext; 
    }


    public static function read_docx($filename){

        $striped_content = '';
        $content = '';

        $zip = zip_open($filename);

        if (!$zip || is_numeric($zip)) return false;

        while ($zip_entry = zip_read($zip)) {

            if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

            if (zip_entry_name($zip_entry) != "word/document.xml") continue;

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

            zip_entry_close($zip_entry);
        }// end while

        zip_close($zip);

        $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
        $content = str_replace('</w:r></w:p>', "\r\n", $content);
        $striped_content = strip_tags($content);

        return nl2br($striped_content);
    }

    public static function getRawWordText($filename) {
        if(file_exists($filename)) {
            if(($fh = fopen($filename, 'r')) !== false ) {
                $headers = fread($fh, 0xA00);
                $n1 = ( ord($headers[0x21C]) - 1 );// 1 = (ord(n)*1) ; Document has from 0 to 255 characters
                $n2 = ( ( ord($headers[0x21D]) - 8 ) * 256 );// 1 = ((ord(n)-8)*256) ; Document has from 256 to 63743 characters
                $n3 = ( ( ord($headers[0x21E]) * 256 ) * 256 );// 1 = ((ord(n)*256)*256) ; Document has from 63744 to 16775423 characters
                $n4 = ( ( ( ord($headers[0x21F]) * 256 ) * 256 ) * 256 );// 1 = (((ord(n)*256)*256)*256) ; Document has from 16775424 to 4294965504 characters
                $textLength = ($n1 + $n2 + $n3 + $n4);// Total length of text in the document
                $extracted_plaintext = fread($fh, $textLength);
                $extracted_plaintext = mb_convert_encoding($extracted_plaintext,'UTF-8');
                 // if you want to see your paragraphs in a new line, do this
                 // return nl2br($extracted_plaintext);
                 return nl2br($extracted_plaintext);
            } else {
                return false;
            }
        } else {
            return false;
        }  
    }


    /**
     * Returns the size of a file without downloading it, or -1 if the file
     * size could not be determined.
     *
     * @param $url - The location of the remote file to download. Cannot
     * be null or empty.
     *
     * @return The size of the file referenced by $url, or -1 if the size
     * could not be determined.
     */
    public static function curl_get_file_size( $url ) {
      // Assume failure.
      $result = -1;
      $success = false;
      $userAgent = 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0';

      $curl = curl_init( $url );

      // Issue a HEAD request and follow any redirects.
      curl_setopt( $curl, CURLOPT_NOBODY, true );
      curl_setopt( $curl, CURLOPT_HEADER, true );
      curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
      curl_setopt( $curl, CURLOPT_USERAGENT, $userAgent );
      curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 

      $data = curl_exec( $curl );
      curl_close( $curl );

      if( $data ) {
        $content_length = "unknown";
        $status = "unknown";

        if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
          $status = (int)$matches[1];
        }

        if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
          $content_length = (int)$matches[1];
        }

        // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
        if( $status == 200 || ($status > 300 && $status <= 308) ) {
          $success = true;
          $result = $content_length;
        }
      }

      if( ($result =='unknown' || $result == -1) && $success){
        $tmpfile = base_path() .'/storage/app/'.str_random(8) .'.chk';
        $fp = fopen ($tmpfile, 'w+');//This is the file where we save the    information
        $ch = curl_init($url);//Here is the file we are downloading, replace spaces with %20
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
        curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent );
        curl_exec($ch); // get curl response
        curl_close($ch);
        fclose($fp);
        $result = filesize($tmpfile);
        unlink($tmpfile);
      } 

      return $result;
    }


}