<?php namespace App\Jobs; 

use DB;
use App\Models\FormatTitle;
use Carbon\Carbon;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;


class UploadJob extends Job implements SelfHandling, ShouldBeQueued
{
	use SerializesModels;
	
    protected $path; 
    protected $minutes;

    public function __construct($path, $minutes)
    {
    	$this->path = $path; 
    	$this->minutes = $minutes;
    }

    public function handle()
    {
    	$this->imporFile($this->path, $this->minutes);
    }


    protected function imporFile($path, $minutes)
    {
     if(!file_exists($path)){
        return false;
     }

      $index = 1;
      $data = [];
      if (($handle = fopen($path, "r")) !== FALSE) {
          while (($row = fgetcsv($handle, 1000, ";")) !== FALSE) {
              if($index){
                $data[] = [
                  'title' => $row[0]
                ];
              }
              $index++;
          }
          fclose($handle);
      }

      return $this->generateData($data, $minutes);
    }


    protected function generateData($data,$minutes)
    {
        $format = new FormatTitle; 
        $result = 0; 
        foreach($data as $row){
            $row['title'] = $format->safe_string_insert($row['title'], 'title');
            $row['slug'] = str_slug($row['title']); 
            $row['created_at'] = Carbon::now()->addMinutes((rand(1, $minutes)))->format('Y-m-d H:i:s'); 
            $found = DB::table('recent_querys')->where('slug', $row['slug'])->first(); 
            if(!$found){
                if(!$this->badWord($row['title'])){
                  Db::table('recent_querys')->insert($row);
                  $result++;                  
                }

            }
        }

        return $result;
    }

    private function badWord($str)
    {
      $bad = "death,dead,deceased,demise,die,dying,expire,fatal stroke,past away,perished,cocaine,drugs,heroin,marijuana,medication,morphine,overdose,oxycodone,oxycontin,pharmaceutical,pharmacy,shit,piss,fuck,tits,drown,drowned,electrocuted,electrocution,execution,killed,killer,manslaughter,miscarriage,murder,murdered,poisoned,poisoning,slaughtered,strangler,strangulation,suffocation,suicide,abortion,aneurysm,bum,crash,cancer,cerebral accident,desanguinated,disfigured,embolism,hemorrhage,horror,maimed,paralyzed,stroke,erection,masturbation,pedophile,penis,porn,pussy,squirting,pussy,schlong,sex,xx,xxx,xxxx,squirting,squirt,blowjob,public sex,p0rn,memek,ngentot,itil,kontol,burial,casket,funeral,attack,bomb,bomber,incerated,jail,prison,terrorist,casino,gambling,google,las vegas,video poker,poker,chip poker,ass,asshole,anal,creampie,bukkake,rapid,rapidshare,megaupload,hotfile";
      $arr = explode(',', $bad); 

      $result = false; 
      foreach($arr as $find){
          if(strpos(strtolower($str), $find)) $result = true;
      }

      return $result;

    }

}
