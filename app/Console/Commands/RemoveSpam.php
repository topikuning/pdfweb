<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\FuncCollection;
use DB;
use Storage;

class RemoveSpam extends Command {

  protected $name = 'content:removespam';

  protected $description = 'Remove spam content manually';

  public function __construct()
  {
    parent::__construct();
  }

  public function fire()
  {
    ini_set('memory_limit', '256M');
    $this->info("run spammer destroy");
    $last_id = 0; 
    if(Storage::exists('last_spam_id')) $last_id = Storage::get('last_spam_id','0'); 
    $reset = $this->argument('reset'); 
    if($reset) $last_id = '0'; 
    $this->info('last id '. $last_id);

    $data = DB::table('posts')->orderBy('id')->where('id','>',$last_id)->get(); 
    foreach($data as $row){
      $this->info('===> read record ======>');
      $this->info('==> ID '. $row->id);
      $this->info($row->title);
      $this->info('check file size');
      $this->info($row->pdf_file);
      $size = FuncCollection::curl_get_file_size($row->pdf_file); 
      $asize = config('site.min_file_size'); 
      $this->info('current file size : '.$size); 
      $this->info('minimum file size : '. $asize); 
      if($size < $asize){
        $this->error('file size is under allowed file size, destroy');
        DB::table('posts')->where('id',$row->id)->delete();
      }else{
        $this->info('file is ok!');
      }
      Storage::put('last_spam_id', $row->id);
      $this->info('sleep 1 second');
      sleep(1);
    }

    //$this->call('sitemap:update',['reset' => 'true']);
    $this->info("done..");
  }





  protected function getArguments()
  {
    return [
      ['reset', InputArgument::OPTIONAL, 
        'only reset the counter of last id', null],
    ];
  }

  protected function getOptions()
  {
    return [
      //['file', null, InputOption::VALUE_REQUIRED, 
      //  'file name in folder import.', null],
    ];
  }

}