<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\FuncCollection;
use DB;
use Storage;

class PingSE extends Command {

  protected $name = 'content:pingse';

  protected $description = 'Ping search engine';

  public function __construct()
  {
    parent::__construct();
  }

  public function fire()
  {
    $this->info('start notify search engine'); 
    $this->ping_search_engines();
    $this->info("done..");
  }

  private function ping_search_engines () 
  {
    $sitemap = config('site.site_url') . '/sitemap.xml';
    $engines = array();
    $engines['www.google.com'] = '/webmasters/sitemaps/ping?sitemap=' . urlencode($sitemap); // google
    //$engines['submissions.ask.com'] = '/ping?sitemap=' . urlencode($sitemap); // ask
    $engines['www.bing.com'] = '/webmaster/ping.aspx?sitemap=' . urlencode($sitemap); // bing
    foreach ($engines as $host => $value) {
        $this->info('pinging '. $host);
        $url = $host . $value;
        $this->info($url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $this->info(' status code ' .$httpCode);
        $this->info('');
    }
  }





  protected function getArguments()
  {
    return [
      //['reset', InputArgument::OPTIONAL, 
      //  'only reset the counter of last id', null],
    ];
  }

  protected function getOptions()
  {
    return [
      //['file', null, InputOption::VALUE_REQUIRED, 
      //  'file name in folder import.', null],
    ];
  }

}