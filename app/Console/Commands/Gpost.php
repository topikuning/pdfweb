<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;
use Storage; 
use Carbon\Carbon;
use App\Models\FormatTitle;

class Gpost extends Command {

  protected $name = 'content:gpost';

  protected $description = 'create recent_querys with add minutes(x) from now';

  public function __construct()
  {
    parent::__construct();
  }

  public function fire()
  {
    $this->info("run insert post command");
    $this->runCommand(); 
    $this->info("done..");
  }

  private function runCommand()
  {
     $minutes = $this->option('minutes');
     if(!$minutes) {
        //$this->error('minutes option not defined. please add --minutes x'); 
        //return false;
        $minutes = 60*24;
        
     }

     $limit = 30000; 
     $start = 0; 

     if(Storage::exists('last_recent_id')){
        $start = Storage::get('last_recent_id');
     }

     if($this->argument('reset')){
        $this->info('reset from beginning data'); 
        $start = 0;
     }

     $this->info('take keywords data from '. $start . ' limit '. $limit); 
     $data = DB::table('keywords')->where('id', '>', $start)->take($limit)->get(); 
     $this->info('');
     $formatTitle = new FormatTitle;
     foreach($data as $row){
      $rand = rand(1, $minutes);
      $created_at = Carbon::now()->addMinutes($rand)->addSeconds(rand(1,59))->format('Y-m-d H:i:s'); 
      $slug = str_slug($row->keyword); 
      $found = DB::table('recent_querys')->where('slug', $slug)->first(); 
      if(!$found){
        $data = [
          'title' => $formatTitle->safe_string_insert( $row->keyword, 'title'),
          'slug' => $slug, 
          'created_at' => $created_at
        ];
        DB::table('recent_querys')->insert($data);
        $this->info('adding post with title '. $data['title']);
      }else{
        $this->error('title already exist');
      }
      $start = $row->id;
      $this->info('');
     }


     Storage::put('last_recent_id', $start);

  }


  protected function getArguments()
  {
    return [
      ['reset', InputArgument::OPTIONAL, 
        'reset the start data'],
    ];
  }

  protected function getOptions()
  {
    return [
      ['minutes', null, InputOption::VALUE_OPTIONAL, 
        'x minutes', null],
    ];
  }

}