<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\FuncCollection;
use DB;
use Storage;

class CleanRecent extends Command {

  protected $name = 'content:cleankeyword';

  protected $description = 'clean recent querys from empty keyword';

  public function __construct()
  {
    parent::__construct();
  }

  public function fire()
  {
    ini_set('memory_limit', '256M');
    $this->info("run clean keyword");


    DB::table('recent_querys')->truncate();

    $data = DB::table('posts')->groupBy('keyword')->get(); 
    foreach ($data as $key => $row) {
        $recent = [
          'title' => $row->keyword,
          'slug' => str_slug($row->keyword),
          'created_at' => DB::raw('now()')
        ];
        if(!DB::table('recent_querys')->where('slug','like',$recent['slug'])->first()){
          DB::table('recent_querys')->insert($recent);
          $this->info('insert to recent keywords table '. $recent['title']);
        }
    }

    $this->info("done..");
  }




  protected function getArguments()
  {
    return [
    ];
  }

  protected function getOptions()
  {
    return [
      //['file', null, InputOption::VALUE_REQUIRED, 
      //  'file name in folder import.', null],
    ];
  }

}