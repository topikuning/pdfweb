<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\FuncCollection;
use DB;
use Storage;

class CleanTitle extends Command {

  protected $name = 'content:cleanstring';

  protected $description = 'clean title and abstact title';

  public function __construct()
  {
    parent::__construct();
  }

  public function fire()
  {
    ini_set('memory_limit', '256M');
    $this->info("run clean title");

    $data = DB::table('posts')->get(); 
    foreach ($data as $key => $row) {
      $update =[
        'title' => $this->safe_string_insert($row->title, 'title'),
        'abstract' => $this->safe_string_insert($row->abstract, 'desc')
      ];

      DB::table('posts')->where('id',$row->id)->update($update);
      $this->info('ID '. $row->id); 
      $this->info('Title '. $update['title']);
    }

    $this->info("done..");
  }

 private function utf8_uri_encode( $utf8_string, $length = 0 ) {
  $unicode = '';
  $values = array();
  $num_octets = 1;
  $unicode_length = 0;

  $string_length = strlen( $utf8_string );
  for ($i = 0; $i < $string_length; $i++ ) {

    $value = ord( $utf8_string[ $i ] );

    if ( $value < 128 ) {
      if ( $length && ( $unicode_length >= $length ) )
        break;
      $unicode .= chr($value);
      $unicode_length++;
    } else {
      if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

      $values[] = $value;

      if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
        break;
      if ( count( $values ) == $num_octets ) {
        if ($num_octets == 3) {
          $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
          $unicode_length += 9;
        } else {
          $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
          $unicode_length += 6;
        }

        $values = array();
        $num_octets = 1;
      }
    }
  }

  return $unicode;
}

private function seems_utf8($str) {
  $length = strlen($str);
  for ($i=0; $i < $length; $i++) {
    $c = ord($str[$i]);
    if ($c < 0x80) $n = 0; # 0bbbbbbb
    elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
    elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
    elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
    elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
    elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
    else return false; # Does not match any model
    for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
      if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
        return false;
    }
  }
  return true;
}

private function url_title($title, $separator = '-', $capitalize = FALSE)
{
  $title = strip_tags($title);
  $title = str_replace(array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6"), '', $title);
  // Preserve escaped octets.
  $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
  // Remove percent signs that are not part of an octet.
  $title = str_replace('%', '', $title);
  // Restore octets.
  $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

  if ($this->seems_utf8($title)) {
    if (function_exists('mb_strtolower')) {
      $title = mb_strtolower($title, 'UTF-8');
    }
    $title = $this->utf8_uri_encode($title, 200);
  }

  $title = preg_replace('/\`\~\!\@\#\$\%\^\&\*\(\)\_\+\-\=\<\>\?\:\"\{\}\|\,\.\/\;\[\]/', '', $title);
  $title = str_replace('.', $separator, $title);
  $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
  $title = ($capitalize) ? safe_ucwords($title): $title;
  $title = preg_replace('/\s+/', $separator, $title);
  $title = preg_replace('|-+|', $separator, $title);
  $title = rtrim($title, $separator);
  $title = trim($title);
  $title = stripslashes($title);
  $title = ($capitalize) ? $title: $this->safe_strtolower($title);
  $title = urldecode($title);
  //$title = html_entity_decode($title, ENT_QUOTES, "UTF-8");
  return $title;
}

private function permalink_url ($str, $replace_separator_to_space = false)
{
  $str = $this->url_title($str, '-', false);
  
  if ($replace_separator_to_space)
  {
    $str = str_replace(config('separator'), ' ', $str);
  }

  return $str;
}

private function safe_string ($str)
{
  $str = $this->url_title($str, '-');
  $str = str_replace('-', ' ', $str);
  return $str;
}

private function safe_ucfirst($str)
{
  if ($this->seems_utf8($str))
  {
    if (function_exists('mb_strtoupper'))
    {
      $encoding = "UTF-8";
      $strlen = mb_strlen($str, $encoding);
      $first_char = mb_substr($str, 0, 1, $encoding);
      $then = mb_substr($str, 1, $strlen - 1, $encoding);
      $str = mb_strtoupper($first_char, $encoding) . $then;
    }
  }
  else
  {
    $str = ucfirst($str);
  }

  return $str;
}

private function safe_strtolower($str)
{
  if ($this->seems_utf8($str))
  {
    if (function_exists('mb_convert_case'))
    {
      $str = mb_convert_case($str, MB_CASE_LOWER, "UTF-8");
    }
  }
  else
  {
    $str = strtolower($str);
  }

  return $str;
}

private function safe_ucwords($str)
{
  if ($this->seems_utf8($str))
  {
    if (function_exists('mb_convert_case'))
    {
      $str = mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
    }
  }
  else
  {
    $str = ucwords($str);
  }

  return $str;
}

private function safe_string_insert ($str, $type)
{
  $str = $this->title_case($this->safe_string($str));

  switch ($type) {
    case 'title':
      if (strlen($str) < 3)
      {
        $str = "Untitled Document";
      }
      else if (strlen($str) > 3 && strlen($str) < 8)
      {
        $str = "Document " . $str;
      }
      break;
    case 'desc':
      if (strlen($str) < 3)
      {
        $str = "No Description";
      }
      else if (strlen($str) > 3 && strlen($str) < 8)
      {
        $str = "Document " . $str;
      }
      break;
  }

  return $str;
}

private function bersihkata($kata)
{
  $kata = strip_tags($kata);
  $kata = $this->safe_strtolower($kata);
  $kata = $this->clean_words($kata);
  $kata = $this->permalink_url($kata, ' ');
  $kata = $this->safe_ucwords($kata);
  return $kata;
}

private function title_case ($title)
{
  $regx = '/<(code|var)[^>]*>.*?<\/\1>|<[^>]+>|&\S+;/';
  preg_match_all($regx, $title, $html, PREG_OFFSET_CAPTURE);
  $title = preg_replace ($regx, '', $title);
  $q_left = chr(8216);
  $q_right = chr(8217);
  $double_q = chr(8220);

  preg_match_all ('/[\w\p{L}&`\''. $q_left . $q_right .'"'. $double_q .'\.@:\/\{\(\[<>_]+-? */u', $title, $m1, PREG_OFFSET_CAPTURE);
  foreach ($m1[0] as &$m2) {
    list ($m, $i) = $m2;
    $i = mb_strlen (substr ($title, 0, $i), 'UTF-8');
    
    $m = $i>0 && mb_substr ($title, max (0, $i-2), 1, 'UTF-8') !== ':' && 
      !preg_match ('/[\x{2014}\x{2013}] ?/u', mb_substr ($title, max (0, $i-2), 2, 'UTF-8')) && 
       preg_match ('/^(a(nd?|s|t)?|b(ut|y)|en|for|i[fn]|o[fnr]|t(he|o)|vs?\.?|via)[ \-]/i', $m)
    ? mb_strtolower ($m, 'UTF-8')
    : ( preg_match ('/[\'"_{(\['. $q_left . $double_q .']/u', mb_substr ($title, max (0, $i-1), 3, 'UTF-8'))
    ? mb_substr ($m, 0, 1, 'UTF-8').
      mb_strtoupper (mb_substr ($m, 1, 1, 'UTF-8'), 'UTF-8').
      mb_substr ($m, 2, mb_strlen ($m, 'UTF-8')-2, 'UTF-8')
    : ( preg_match ('/[\])}]/', mb_substr ($title, max (0, $i-1), 3, 'UTF-8')) ||
      preg_match ('/[A-Z]+|&|\w+[._]\w+/u', mb_substr ($m, 1, mb_strlen ($m, 'UTF-8')-1, 'UTF-8'))
    ? $m
    : mb_strtoupper (mb_substr ($m, 0, 1, 'UTF-8'), 'UTF-8').
      mb_substr ($m, 1, mb_strlen ($m, 'UTF-8'), 'UTF-8')
    ));
    
    $title = mb_substr ($title, 0, $i, 'UTF-8').$m. mb_substr ($title, $i+mb_strlen ($m, 'UTF-8'), mb_strlen ($title, 'UTF-8'), 'UTF-8');
  }

  foreach ($html[0] as &$tag) $title = substr_replace ($title, $tag[0], $tag[1], 0);
  return $title;
}
 



  protected function getArguments()
  {
    return [
    ];
  }

  protected function getOptions()
  {
    return [
      //['file', null, InputOption::VALUE_REQUIRED, 
      //  'file name in folder import.', null],
    ];
  }

}