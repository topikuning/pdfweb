<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;
use Storage; 
use Carbon\Carbon;

class UpdatePost extends Command {

  protected $name = 'content:updatepost';

  protected $description = 'Update created_at post with add minutes(x) from now';

  public function __construct()
  {
    parent::__construct();
  }

  public function fire()
  {
    $this->info("run update post command");
    $this->runCommand(); 
    $this->info("done..");
  }

  private function runCommand()
  {
     $minutes = $this->option('minutes');
     if(!$minutes) {
        $this->error('minutes option not defined. please add --minutes x'); 
        return false;
     }

     $limit = 20000; 
     $start = 0; 

     if(Storage::exists('last_update_id')){
        $start = Storage::get('last_update_id');
     }

     if($this->argument('reset')){
        $this->info('reset from beginning data'); 
        $start = 0;
     }

     $this->info('take post data from '. $start . ' limit '. $limit); 
     $data = DB::table('recent_querys')->where('id', '>', $start)->take($limit)->get(); 
     $this->info('');
     foreach($data as $row){
      $rand = rand(1, $minutes);
      $created_at = Carbon::now()->addMinutes($rand)->addSeconds(rand(1,59))->format('Y-m-d H:i:s'); 
      $this->info('update created_at with ' . $created_at . ' where id = '. $row->id ); 
      DB::table('recent_querys')->where('id', $row->id)->update(['created_at' => $created_at]); 
      DB::table('posts')->where('post_id', $row->id)->update(['created_at' => $created_at]);
      $start = $row->id;
      $this->info('');
     }


     Storage::put('last_update_id', $start);

  }


  protected function getArguments()
  {
    return [
      ['reset', InputArgument::OPTIONAL, 
        'reset the start data'],
    ];
  }

  protected function getOptions()
  {
    return [
      ['minutes', null, InputOption::VALUE_REQUIRED, 
        'x minutes', null],
    ];
  }

}